import dhdl.graph._
import dhdl.codegen._
import dhdl.Design

object SimpleMetaPipeline extends Design {

  def main(args: String*) = {
    if (args.size != 2) {
      println("\nUsage: SimpleMetaPipeline <#tileRows> <#tileCols>")
      sys.exit(0)
    }

    val tileRows = args(0).toInt
    val tileCols = args(1).toInt
    val tileSize = tileRows * tileCols

    val matRows = ArgIn("matRows")
    val matCols = ArgIn("matCols")

    // Off-chip memory input
    val matrixIn = OffChipArray("matrixIn", matRows, matCols)

    // Off-chip memory output
    val matrixOut = OffChipArray("matrixOut", matRows, matCols)

    val mpCtr = Ctr((matRows,tileRows), (matCols, tileCols))

    // Stage 1: Load tile from matrixIn
    val tileRAM = BRAM(tileSize, true)  // <-- Programmer should declare double buffer
    val tileLd = TileMemLd(matrixIn, matCols, mpCtr(0), mpCtr(1), tileRAM, tileRows, tileCols)

    // Stage 2: Do something to the tile
    val prodRAM = BRAM(tileSize, true)
    val ctr = Ctr((Const(tileSize),1))
    val data = Ld(tileRAM, ctr(0))
    val prod = Mul(data, mpCtr(0))
    val st = St(prodRAM, ctr(0), prod)
    val pipe = Pipe (ctr, GraphNode(data, prod, st))

    // Stage 3: Store tile back
    val tileSt = TileMemSt(matrixOut, matCols, mpCtr(0), mpCtr(1), prodRAM, tileRows, tileCols)
//    val tileSt = TileMemSt(matrixOut, matCols, mpCtr(0), mpCtr(1), tileRAM, tileRows, tileCols)

    // Wire up mpCtr and the stages as a MetaPipeline
    val top = MetaPipeline(mpCtr, tileLd, pipe, tileSt)
    top
  }
}


