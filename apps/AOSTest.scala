import dhdl.graph._
import dhdl.codegen._
import dhdl.codegen.dot.GraphvizCodegen
import dhdl.analysis.IRPrintPass
import dhdl.DesignTest

import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer

/**
 *  Array-of-structures test: Simple Load-store
 *  loop that loads an OffChipArray declared
 *  as a CompositeType. Each field declared in the
 *  ComposityType is loaded into a separate BRAM.
 */
object AOSTest extends DesignTest {

  def main(args: String*) = {
    if (args.size != 2) {
      println("\nUsage: AOSTest <vecTileSize> <#shouldMP>")
      sys.exit(0)
    }
    val vecSize = ArgIn("vecSize")
    val tileSize = args(0).toInt
    val shouldMetapipeline = if (args(1).toInt == 0) false else true

    // Off-chip memory input
    val aosIn = OffChipArray(CompositeType(List(UInt(), Flt())))("aosIn", vecSize)

    // Off-chip memory output
    val intOut = OffChipArray("intOut", vecSize)
    val fltOut = OffChipArray(Flt())("fltOut", vecSize)

    val seqCtr = Ctr((vecSize,tileSize))

    val intRAM = BRAM(tileSize, shouldMetapipeline)
    val fltRAM = BRAM(Flt(), tileSize, shouldMetapipeline)
    val aosTileLd = TileMemLd(aosIn, vecSize, Const(0), seqCtr(0), List(intRAM, fltRAM), 1, tileSize)

    val intSt = TileMemSt(intOut, vecSize, Const(0), seqCtr(0), intRAM, 1, tileSize)
    val fltSt = TileMemSt(fltOut, vecSize, Const(0), seqCtr(0), fltRAM, 1, tileSize)
    val parSt = Parallel(intSt, fltSt)

    val top = if (shouldMetapipeline) MetaPipeline(seqCtr, aosTileLd, parSt) else Sequential(seqCtr, aosTileLd, parSt)
    top
  }

  def test(args: String*) {
    val tileSize = args(0.toInt)
    setArg("vecSize", 192)
    val data = Array.tabulate(192){i => i}
    setMem("vecIn", data)

    run()

    val out = getMem[Int]("vecIn", 0 until 192)
    compare(out, data)
  }
}


