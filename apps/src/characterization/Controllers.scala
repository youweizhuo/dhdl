import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest}

object CtrChar extends Design {
  def main(args: String*) = {
    val N = args(0).toInt
    val max = List.tabulate(N){i => ArgIn(s"m$i") }
    val ctr = Ctr(max.zipWithIndex:_*)
    Parallel(ctr) // Probably not needed?
  }
}

object ParallelChar extends Design {
  def main(args: String*) = {
    val N = args(0).toInt
    val maxes = List.tabulate(N){i => ArgIn(s"m$i") }
    val ctrs  = List.tabulate(N){i => Ctr((maxes(i),1)) }
    Parallel(ctrs:_*)
  }
}

object SequentialChar extends Design {
  def main(args: String*) = {
    val N = args(0).toInt
    val ctr = Ctr((Const(1),1))
    val dummies = List.fill(N)(DummyCtrlNode())
    Sequential(ctr, dummies:_*)
  }
}

object MetapipelineChar extends Design {
  def main(args: String*) = {
    val N = args(0).toInt
    val ctr = Ctr((Const(1),1))
    val dummies = List.fill(N)(DummyCtrlNode())
    MetaPipeline(ctr, dummies:_*)
  }
}

object PipeTest extends Design {
  def main(args:String*) = {

    val ctr = Ctr((Const(1),1))
    val a = ArgIn(Flt())("a")
    val b = ArgIn(Flt())("b")
    val c = ArgIn(Flt())("c")

    val out = ArgOut("out", Add(Mul(a,b),c))

    Pipe(ctr, GraphNode(out))
  }
}

object TileLd extends Design {
  val T = SInt(32)

  def main(args: String*) = {
    if (args.size != 3) {
      println("\nUsage: TileLd <#lds> <#tileRows> <#tileCols>")
      sys.exit(0)
    }
    val N = args(0).toInt
    val tileRows = args(1).toInt
    val tileCols = args(2).toInt

    val ctr = Ctr((Const(10000),1))

    val a = ArgIn(T)("a")
    val arrayIn = OffChipArray(T)("array", a)

    val dummyRAMs = List.fill(N){ DummyMemNode(T) }
    val tileLds = dummyRAMs.map{ram => TileMemLd(arrayIn, a, Const(0), ctr(0), ram, tileRows, tileCols)}

    val loaders = Parallel(tileLds:_*)

    val argOut = Pipe(Block {
      Array.tabulate(N){i => ArgOut(s"out$i", Ld(dummyRAMs(i), Const(0))) }
    })

    Sequential(ctr, loaders, argOut)
  }
}

object TileSt extends Design {
  def main(args: String*) = {
    if (args.size != 3) {
      println("\nUsage: TileLd <#ld/st> <#tileRows> <#tileCols>")
      sys.exit(0)
    }
    val N = args(0).toInt
    val tileRows = args(1).toInt
    val tileCols = args(2).toInt

    val a = ArgIn("a")
    val in = ArgIn("b")
    val arrayOut = OffChipArray("array", a)
    val ctr = Ctr((Const(10000), 1))

    val dummyRAMs = List.fill(N){ DummyMemNode() }

    val storing = Pipe(Block{dummyRAMs.foreach{ram => St(ram, Const(0), in)} })
    val tileSts = dummyRAMs.map{ram => TileMemSt(arrayOut, a, Const(0), ctr(0), ram, tileRows, tileCols)}
    val tileStores = Parallel(tileSts:_*)

    Sequential(ctr, storing, tileStores)
  }
}

object BRAMTest extends Design {
  def main(args: String*) = {
    val N = args(0).toInt

    val ins = List.tabulate(N){i => ArgIn(s"in$i") }
    val ram = List.tabulate(N){i => BRAM(32, true) }
    val sts = List.tabulate(N){i => St(ram(i), Const(0), ins(i)) }
    val lds = List.tabulate(N){i => Ld(ram(i), Const(0)) }
    val outs = List.tabulate(N){i => ArgOut(s"out$i", lds(i)) }

    val pipe = Pipe((ins ++ sts ++ lds ++ outs):_*)
    val ctr = Ctr((Const(5),1))
    val meta = MetaPipeline(ctr, pipe)
    meta
  }
}
