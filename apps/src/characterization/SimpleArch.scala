import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design,DesignTest}

object SimpleMap1 extends DesignTest {
  def main(args: String*) = {
    val N = ArgIn("N")
    bound(N, 128)

    val i = Ctr((N,32))
    val ii = Ctr((Const(32),1))

    val a = OffChipArray("a", N);   val aBlk = BRAM(32)
    val b = OffChipArray("b", N);   val bBlk = BRAM(32)
    val c = OffChipArray("c", N);   val cBlk = BRAM(32)
    val o = OffChipArray("o", N);   val oBlk = BRAM(32)
    val aLd = TileMemLd(a, Const(1), i(0), Const(0), aBlk, 32, 1)
    val bLd = TileMemLd(b, Const(1), i(0), Const(0), bBlk, 32, 1)
    val cLd = TileMemLd(c, Const(1), i(0), Const(0), cBlk, 32, 1)
    val oSt = TileMemSt(o, Const(1), i(0), Const(0), oBlk, 32, 1)

    val mapFunc = Block {
      val aa = Ld(aBlk, ii(0))
      val bb = Ld(bBlk, ii(0))
      val cc = Ld(cBlk, ii(0))
      val oo = St(oBlk, ii(0), Add(Mul(aa,bb),cc))
    }

    Sequential(i, Parallel(aLd,bLd,cLd), Pipe(ii, mapFunc), oSt)
  }
  def test(args: String*) {
    val N = 128
    setArg("N", N)

    val a = Array.tabulate(N){i => util.Random.nextInt(200) }
    val b = Array.tabulate(N){i => util.Random.nextInt(200) }
    val c = Array.tabulate(N){i => util.Random.nextInt(200) }
    setMem("a", a)
    setMem("b", b)
    setMem("c", c)
    run()
    val o = getMem[Int]("o", 0 until N)
    val oGold = Array.tabulate(N){i => a(i) * b(i) + c(i) }
    compare(o, oGold)
  }
}

object SimpleMap2 extends Design {
  def main(args: String*) = {
    val N = ArgIn("N"); bound(N, 128)
    val X = ArgIn("X")

    val i = Ctr((N,32))
    val ii = Ctr((Const(32),1))

    val a = OffChipArray("a", N);   val aBlk = BRAM(32)
    val b = OffChipArray("b", N);   val bBlk = BRAM(32)
    val c = OffChipArray("c", N);   val cBlk = BRAM(32)
    val o = OffChipArray("o", N);   val oBlk = BRAM(32)
    val aLd = TileMemLd(a, Const(1), i(0), Const(0), aBlk, 32, 1)
    val bLd = TileMemLd(b, Const(1), i(0), Const(0), bBlk, 32, 1)
    val cLd = TileMemLd(c, Const(1), i(0), Const(0), cBlk, 32, 1)
    val oSt = TileMemSt(o, Const(1), i(0), Const(0), oBlk, 32, 1)

    val mapFunc = Block {
      val aa = Ld(aBlk, ii(0))
      val bb = Ld(bBlk, ii(0))
      val cc = Ld(cBlk, ii(0))
      val aa_bb = Add(aa,bb) // a + b
      val oo = St(oBlk, ii(0), Add(Mul(aa_bb,aa_bb), Add(Mul(cc,cc), X) )) // (a + b)^2 + c^2 + X
    }

    Sequential(i, Parallel(aLd,bLd,cLd), Pipe(ii, mapFunc), oSt)
  }
}
