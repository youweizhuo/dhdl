import dhdl.graph._
import dhdl.codegen._
import dhdl.DesignTest

object SumRowsSimple extends DesignTest {

  def main(args: String*) = {
    if (args.size != 1) {
      println("\nUsage: SumRowsSimple <#tileSize (should be the same as rowSize)>")
      sys.exit(0)
    }
    val tileSize = args(0).toInt

    // Off-chip memory input
    val R = ArgIn("R")
    val C = ArgIn("C")
    val matrix = OffChipArray("matrix", R, C)
    val rowSum = OffChipArray("rowSum", C)

    val seqCtr = Ctr((R,1))

    val rowRAM = BRAM(tileSize, true)

    // 1. Load row from off-chip memory
    val rowLd = TileMemLd(matrix, C, seqCtr(0), Const(0), rowRAM, 1, tileSize)

    // 2. Accumulate into a BRAM
    val accumRAM = BRAM(tileSize)
    val accumCtr = Ctr((Const(tileSize), 1))
    val v1 = Ld(rowRAM, accumCtr(0))
    val v2 = Ld(accumRAM, accumCtr(0))
    val sum = Add(v1, v2)
    val st = St(accumRAM, accumCtr(0), sum)
//    val accumPipe = Pipe(accumCtr, GraphNode(v1, v2, sum, st))
    val accumPipe = CollectReduceTree(rowRAM, accumRAM, Pipe(accumCtr, GraphNode(v1, v2, sum, st)))

    // Wire (1) and (2) in a Sequential controller
    val allRowSum = MetaPipeline(seqCtr, rowLd, accumPipe)

    // Once all rows have been added, store accumRAM out
    val rowSt = TileMemSt(rowSum, C, Const(0), Const(0), accumRAM, 1, tileSize)

    // Wire up allRowSum and rowSt in a top Sequential CtrlNode
    val top = Sequential(allRowSum, rowSt)
    top
  }

 def test(args: String*) {
    val tileSize = args(0).toInt
    // Setup
    val R = 10*tileSize; val C = tileSize; // C must equal tile size for this design
    setArg("R", R)
    setArg("C", C)
    val matrix = Array.tabulate(R){i => Array.tabulate(C){j => i + j}}
    setMem("matrix", matrix.flatten)

    val rowSumsGold = Array.tabulate(C){j => Array.tabulate(R){i => matrix(i)(j) }.sum }

    run()

    // Evaluation
    val rowSums = getMem[Int]("rowSum", 0 until C)

    compare(rowSums, rowSumsGold)
    println("Row sums: ")
    println(rowSums.mkString(", "))
  }

}
