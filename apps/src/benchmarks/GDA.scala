import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design,DesignTest,DSE}

object GDA extends GDADesign
object GDATest extends GDADesign with DesignTest {
  def test(args: String*) {
    rTileSize = args(0).toInt

    val r = rTileSize * 2
    val c = cTileSize
    setArg("r", r)
    setArg("c", c)

    val x = Array.tabulate(r){i => Array.tabulate(c){j => util.Random.nextInt(100)}}
    val y = Array.tabulate(r){i => util.Random.nextBoolean()}
    val mu0 = Array.tabulate(c){i => util.Random.nextDouble()*100}
    val mu1 = Array.tabulate(c){i => util.Random.nextDouble()*100}
    //println("x")
    //x.foreach(row => println(row.mkString(" ")))
    //println("y")
    //println(y.mkString(" "))
    //println("mu0")
    //println(mu0.mkString(" "))
    //println("mu1")
    //println(mu1.mkString(" "))

    setMem("x", x.flatten)
    setMem("y", y)
    setMem("mu0", mu0)
    setMem("mu1", mu1)

    run()

    var gold_sigma = Array.fill(c,c){0.0}
    y.zipWithIndex.foreach{case (yi, rIdx) =>
      val mu_temp = if (yi) (x(rIdx),mu1).zipped.map(_-_)
                      else (x(rIdx), mu0).zipped.map(_-_)
      val outProd = Array.tabulate(c){i =>
                      Array.tabulate(c){j=> mu_temp(i)*mu_temp(j)}}
      gold_sigma = Array.tabulate(c){i =>
        Array.tabulate(c){j => gold_sigma(i)(j) + outProd(i)(j)}}
    }

    val sigma = getMem[Double]("sigma", 0 until c*c)
    //println("gold_sigma:")
    //println(gold_sigma.flatten.mkString(" "))
    //println("sigma:")
    //println(sigma.mkString(" "))
    compare(gold_sigma.flatten, sigma)
  }
}
trait GDADesign extends Design {

	var rTileSize:Int = 0;
	val cTileSize:Int = 96;

  var subLoopPar = 1
  var prodLoopPar = 1
  var accumLoopPar = 1

  var innerLoopPar = 1
  var outerLoopPar = 1
  var shouldMpInner = false
  var shouldMpOuter = false
  val optionalReduceLoopPar = 1

  def tiledOutProd(vec1B: BRAM, vec2B:BRAM, tileSize:Int, outputDblBuf:Boolean, par: Int = 1, tp:Type = UInt()) =  {
    if (vec1B.t!=tp || vec2B.t!=tp)
      throw new Exception("vectors to perform outer product must have the same type as output vector!")
    val resultB = BRAM(tp, tileSize*tileSize, outputDblBuf)

    val opCtrs = Ctr(List(1, par))((Const(tileSize), 1), (Const(tileSize), 1))
    val mapNode = Block({
      val v1xv2 = Mul(Ld(vec1B, opCtrs(0)), Ld(vec2B, opCtrs(1)))
      val resRowBase = Mul(Const(tileSize), opCtrs(0))
      val resAddr = Add(resRowBase, opCtrs(1))
      val resultBSt = St(resultB, resAddr, v1xv2)
    })
    val pipe = Pipe(opCtrs, mapNode)
    (pipe, resultB)
  }

  def main(args: String*) = {
    if (args.size < 8) {
      println("usage: gda <rtilesize> <subLoopPar> <prodLoopPar> <accumLoopPar> <innerLoopPar> <outerLoopPar> <mpInner> <mpOuter>")
      sys.exit(0)
		}

		rTileSize = args(0).toInt
		//cTileSize = args(1).toInt
    subLoopPar = args(1).toInt
    prodLoopPar = args(2).toInt
    accumLoopPar = args(3).toInt
    innerLoopPar = args(4).toInt
    outerLoopPar = args(5).toInt
    shouldMpInner = args(6) == "true"
    shouldMpOuter = args(7) == "true"

    val r = ArgIn("r"); bound(r, 360000)
    val c = ArgIn("c"); bound(c, 96)

    val	x = OffChipArray(FloatPt())("x", r, c)
    val y = OffChipArray(Bool())("y", r)

		// Assume fit on chip and c=cTileSize
		val mu0 = OffChipArray(FloatPt())("mu0", c)
		val mu1 = OffChipArray(FloatPt())("mu1", c)
		val mu0B = BRAM(FloatPt(), "mu0B", cTileSize, false)
		val mu1B = BRAM(FloatPt(), "mu1B", cTileSize, false)
    val mu0Ld = TileMemLd(mu0, Const(cTileSize), Const(0), Const(0), mu0B, 1, cTileSize)
    val mu1Ld = TileMemLd(mu1, Const(cTileSize), Const(0), Const(0), mu1B, 1, cTileSize)

		val (meta_tile, sigmaB) = {
			val rTileCtr = Ctr(List(outerLoopPar))((r, rTileSize))

      val (ldStage, xBList, yBList) = {
        val (node, memList) = replicate(outerLoopPar) { outpid =>
          val xB = BRAM(FloatPt(), s"xB$outpid", rTileSize*cTileSize, shouldMpOuter)
          val yB = BRAM(Bool(), s"yB$outpid", rTileSize, shouldMpOuter)
          val xLd = TileMemLd(x, c, rTileCtr(0)(outpid), Const(0), xB, rTileSize, cTileSize)
          val yLd = TileMemLd(y, c, Const(0), rTileCtr(0)(outpid), yB, 1, rTileSize)
          (Parallel(xLd, yLd), List(xB, yB))
        }
        val xBList = memList.map {_(0)}.toList
        val yBList = memList.map {_(1)}.toList
        (node, xBList, yBList)
      }

			val (meta_outProdAccum, sigmaBList) = {
        replicate(outerLoopPar) { outpid =>
          val rCtr = Ctr(List(innerLoopPar))((Const(rTileSize), 1))

          val (pipe_xSubMu, xSubMuBList) = replicate(innerLoopPar) { pid =>
            val (pipe_xSubMu, xSubMuB) = {
              val xSubMuB = BRAM(FloatPt(), s"xSubMuB_${outpid}_${pid}", cTileSize, shouldMpInner)
              val cCtr = Ctr(List(subLoopPar))((Const(cTileSize), 1))
              (Pipe(cCtr, Block({
                val mu0Pt = Ld(mu0B, cCtr(0))
                val mu1Pt = Ld(mu1B, cCtr(0))
                val y = Ld(yBList(outpid), rCtr(0)(pid))
                val xrowBase = Mul(Const(cTileSize), rCtr(0)(pid))
                val xAddr = Add(xrowBase, cCtr(0))
                val xPt = Ld(xBList(outpid), xAddr)
                val xSubMu0 = Sub(xPt, mu0Pt)
                val xSubMu1 = Sub(xPt, mu1Pt)
                val subSel = Eq(y, Const(false))
                val xSubMu = Mux(subSel, xSubMu0, xSubMu1)
                St(xSubMuB, cCtr(0), xSubMu)
              })), xSubMuB)
            }
            (pipe_xSubMu, List(xSubMuB))
          }


          val (pipe_opxSubMu, opxSubMuBList) = replicate(innerLoopPar) { pid =>
            val xSubMuB = xSubMuBList(pid)(0).asInstanceOf[BRAM]
            val (pipe_opxSubMu, opxSubMuB) = tiledOutProd(xSubMuB, xSubMuB, cTileSize, shouldMpInner, prodLoopPar, FloatPt())
            (pipe_opxSubMu, List(opxSubMuB))
          }

          val (pipe_accumSigma, sigmaB) = {
            val cCtrs = Ctr(List(accumLoopPar))((Const(cTileSize*cTileSize), 1))
            val sigmaB = BRAM(FloatPt(), s"sigmaB_${outpid}", cTileSize*cTileSize, false)
            sigmaB.setAccum()

            val redpipe = reducePipeAccum(innerLoopPar, cCtrs, sigmaB) { pid =>
              Ld(opxSubMuBList(pid)(0), cCtrs(0))
            } { (a,b) => Add(a,b) }

            (redpipe, sigmaB)
          }


          (if (shouldMpInner) MetaPipeline(rCtr, pipe_xSubMu, pipe_opxSubMu, pipe_accumSigma) else Sequential(rCtr, pipe_xSubMu, pipe_opxSubMu, pipe_accumSigma), List(sigmaB))
        }
			}

      // For all buffers in sigma, reduce using the same redpipe as within the inner mp
     val (pipe_accumSigmaOuter, sigmaB) = {
       if (sigmaBList.size > 1) {
         val cCtrs = Ctr(List(optionalReduceLoopPar))((Const(cTileSize*cTileSize), 1))
         val sigmaB = BRAM(FloatPt(), "sigmaB", cTileSize*cTileSize, false)
         sigmaB.setAccum()

         val redpipe = reducePipeAccum(outerLoopPar, cCtrs, sigmaB) { pid =>
           Ld(sigmaBList(pid)(0), cCtrs(0))
         } { (a,b) => Add(a,b) }
         (Some(redpipe), sigmaB)
       } else {
          (None, sigmaBList(0)(0))
       }
    }

     val stages = List(ldStage, meta_outProdAccum) ++ pipe_accumSigmaOuter.toList
			(if (shouldMpOuter) MetaPipeline(rTileCtr, stages:_*) else Sequential(rTileCtr, stages:_*),
				sigmaB)
		}

		val sigma = OffChipArray(FloatPt())("sigma", c, c)
    val sigmaSt = TileMemSt(sigma, c, Const(0), Const(0), sigmaB, cTileSize, cTileSize)

		val top = Sequential(Parallel(mu0Ld, mu1Ld), meta_tile, sigmaSt)
		top
	}
}

object GDADSE extends DSE {
  val name = "GDA"
  val makeDesign = () => new GDADesign{}
  //override val verbose = true
  val RTileSize = Discrete("PtTileSize", (6 to 960 by 6).toList :+ 1)   // 24
  val SubLoopPar = Discrete("SubLoopPar", (1 to 9216).toList.filter(x => 9216 % x == 0 && x < 100)) // 33
  val ProdLoopPar = Discrete("ProdLoopPar", (1 to 9216).toList.filter(x => 9216 % x == 0 && x < 100)) // 33
  val AccumLoopPar = Discrete("AccumLoopPar", (1 to 9216).toList.filter(x => 9216 % x == 0 && x < 100)) // 33
  val InnerPar = Discrete("InnerPar", 1 to 20)                            // 10
  val OuterPar = Discrete("OuterPar", 1 to 4)                             // 4
  val MetapipeInner = Category("MetapipeInner", true, false)              // 2
  val MetapipeOuter = Category("MetapipeOuter", true, false)              // 2

  val DimSize = Discrete("DimSize", List(96)) // Not actually a parameter, just used to narrow space
  val DimSqrd = Discrete("DimSqrd", List(96*96))
  (SubLoopPar divides DimSize)
  (AccumLoopPar divides DimSqrd)
  (InnerPar <= RTileSize) and (InnerPar divides RTileSize)

  override def isLegal(args: Point) = if (super.isLegal(args)) {
    val sub = args(1).toInt; val prod = args(2).toInt; val accum = args(3).toInt

    ((sub == prod && prod == accum) || (sub == prod && accum == 1) ||
     (sub == accum && prod == 1)    || (accum == prod && sub == 1) ||
     (sub == 1 && prod == 1) || (prod == 1 && accum == 1) || (accum == 1 && sub == 1))
  } else false
}
