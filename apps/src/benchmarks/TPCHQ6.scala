import dhdl.graph._
import dhdl.codegen._
import dhdl.{DSE,Design,DesignTest}

object TPCHQ6 extends TPCHQ6Design
object TPCHQ6Test extends TPCHQ6Design with DesignTest {
  def test(args: String*) {
    val tileSize = args(0).toInt
    val N = 10*tileSize
    val minDate = 64; val maxDate = 80

    setArg("arraySize", N);  setArg("minDate", minDate);  setArg("maxDate", maxDate)

    val dates = Array.tabulate(N){i => util.Random.nextInt(35) + 50};       setMem("dates", dates)
    val quants = Array.tabulate(N){i => util.Random.nextInt(50)};           setMem("quants", quants)
    val discts = Array.tabulate(N){i => util.Random.nextDouble() * 0.1 };   setMem("discounts", discts)
    val prices = Array.tabulate(N){i => util.Random.nextDouble() * 1000 };  setMem("prices", prices)

    val conds = Array.tabulate(N){i => dates(i) > minDate && dates(i) < maxDate && quants(i) < 24 &&
                                       discts(i) >= 0.05 && discts(i) <= 0.07}

    val gold = conds.zipWithIndex.filter(x=>x._1).map{i => prices(i._2) * discts(i._2) }.sum

    run()

    val result = getArg[Double]("result")

    println(s"$result =?= $gold")
    compare(result, gold)
  }
}
class TPCHQ6Design extends Design {
  def main(args: String*) = {
    if (args.size != 4) {
      println("\nUsage: TPCHQ6 <#tileSize> <#parouter> <#parpipe> <metapipeline>")
      sys.exit(0)
    }
    val tileSize = args(0).toInt
    val par_outer = args(1).toInt
    val par_pipe = args(2).toInt
    val shouldMetapipeline = args(3) == "true"

		val arraySize = ArgIn("arraySize"); bound(arraySize, 9993600)
		val minDate = ArgIn("minDate")
		val maxDate = ArgIn("maxDate")

    val dates = OffChipArray("dates", arraySize)
    val quants = OffChipArray("quants", arraySize)
    val discounts = OffChipArray(FloatPt())("discounts", arraySize)
    val prices = OffChipArray(FloatPt())("prices", arraySize)

		val tileCtr = Ctr(List(par_outer))((arraySize, tileSize))

    val s1 = replicate(par_outer) { i =>
      val td_dates = BRAM(tileSize, shouldMetapipeline)
      val td_quants = BRAM(tileSize, shouldMetapipeline)
      val td_discounts = BRAM(FloatPt(), tileSize, shouldMetapipeline)
      val td_prices = BRAM(FloatPt(), tileSize, shouldMetapipeline)

      val td_datesLd = TileMemLd(dates, arraySize, Const(0), tileCtr(0)(i), td_dates, 1, tileSize)
      val td_quantsLd = TileMemLd(quants, arraySize, Const(0), tileCtr(0)(i), td_quants, 1, tileSize)
      val td_discountsLd = TileMemLd(discounts, arraySize, Const(0), tileCtr(0)(i), td_discounts, 1, tileSize)
      val td_pricesLd = TileMemLd(prices, arraySize, Const(0), tileCtr(0)(i), td_prices, 1, tileSize)
      val para_offChipLd = Parallel(td_datesLd, td_quantsLd, td_discountsLd, td_pricesLd)
      (para_offChipLd, List(td_dates, td_quants, td_discounts, td_prices))
    }
    val td_dates = s1._2.map { l => l(0) }
    val td_quants = s1._2.map { l => l(1) }
    val td_discounts = s1._2.map { l => l(2) }
    val td_prices = s1._2.map { l => l(3) }

		val ctr = Ctr(List(par_pipe))((Const(tileSize),1))
		val accum = Reg(FloatPt())
    val s2 = reducePipe(par_outer, ctr, accum) { i =>
      val date = Ld(td_dates(i), ctr(0))
      val quant = Ld(td_quants(i), ctr(0))
      val discount = Ld(td_discounts(i), ctr(0))
      val price = Ld(td_prices(i), ctr(0))
      val mul = Mul(price, discount)

      val dateGtMin = Gt(date, minDate)
      val dateLtMax = Lt(date, maxDate)
      val discountGe = Ge(discount, Const(0.05f))
      val discountLe = Le(discount, Const(0.07f))
      val quantLt = Lt(quant, Const(24))
      val valid = And(dateGtMin, dateLtMax, discountGe, discountLe, quantLt)
      val accumMux = Mux(valid, mul, Const(0.0f))
      accumMux
    } { (a,b) => Add(a,b) }

    val result = ArgOut("result",accum)

  	val top = if (shouldMetapipeline) MetaPipeline(tileCtr, s1._1, s2) else Sequential(tileCtr, s1._1, s2)
    accum.setParent(top)
    result.setParent(top)

    top
  }
}

object TPCHQ6DSE extends DSE {
  val name = "TPCHQ6"
  val makeDesign = () => new TPCHQ6Design{}
  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
  val OuterPar = Discrete("OuterPar", 1 to 3)
  val InnerPar = Discrete("InnerPar", 1 to 19200)
  val Metapipe = Category("Metapipe", true, false)

  (InnerPar <= TileSize) and (InnerPar divides TileSize)
}