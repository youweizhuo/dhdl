import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}

object LogReg extends LogRegDesign
trait LogRegDesign extends DesignTest {

  val T = Flt()
  var tileSize = 96
  var innerPar = 1
  var shouldMetapipeline = false
  var D = 16

  def main(args: String*) = {
    if (args.size != 4) {
      println("\nUsage: LogReg <#tileN> <#D> <#ipar> <metapipeline?>")
      sys.exit(0)
    }

    tileSize = args(0).toInt
    D = args(1).toInt
    innerPar = args(2).toInt
    shouldMetapipeline = args(3) == "true"

    // Off-chip memory input
    val N = ArgIn("N"); bound(N, 9993600)

    val x = OffChipArray(T)("x", N, Const(D))
    val y = OffChipArray(T)("y", N)

    val theta = BRAM(T, D, false)

    val rCtr = Ctr((N, tileSize))
    val r = rCtr(0)

    // Gradient: Process all rows {
      // 1. Load tiles from off-chip memory {
      val bx = BRAM(T, tileSize*D, shouldMetapipeline)
      val by = BRAM(T, tileSize, shouldMetapipeline)
      val bxLd = TileMemLd(x, Const(D), r, Const(0), bx, tileSize, D)
      val byLd = TileMemLd(y, Const(D), Const(0), r, by, 1, tileSize)
      val offChipLoad = Parallel(bxLd, byLd)
      // End off-chip load }

      // 2. Process one row at a time {
      val rrCtr = Ctr((Const(tileSize), 1))
      val rr = rrCtr(0)

        // Pipe 1: Dot product pipe: Row rr, theta
        val jCtr = Ctr(List(innerPar))((Const(D), 1))
        val j = jCtr(0)
        val rowaddr = Mul(rr, Const(D))
        val addr = Add(rowaddr, j)
        val rval = Ld(bx, addr)
        val thetaVal = Ld(theta, j)
        val mul = Mul(rval, thetaVal)
        val dotAccum = Reg(T)
        val dotSum = Add(mul, dotAccum)
        dotAccum.write(dotSum)
        val dotPipe = Pipe(jCtr,
          GraphNode(rowaddr, addr, rval, thetaVal, mul),
          ReduceTree(mul, dotAccum, dotSum)
        )
        dotAccum.setParent(dotPipe)

        // Pipe 2: Unit pipe: y(rr) - sigmoid(dotAccum)
        val yLd = Ld(by, rr)
        val negativeT = Sub(Const(0.0f), dotAccum)
        val exp = Exp(negativeT)
        val denom = Add(Const(1.0f), exp)
        val sigmoid = Div(Const(1.0f), denom)
        val diff = Sub(yLd, sigmoid)
        val pipe2Res = Reg(T)
        pipe2Res.write(diff)
        val pipe2 = Pipe(yLd, negativeT, exp, denom, sigmoid, diff, pipe2Res)
        pipe2Res.setParent(pipe2)

        // Pipe 3: row rr * (y(rr) - sigmoid)
        val subRAM = BRAM(T, D, shouldMetapipeline)
        val kCtr = Ctr(List(innerPar))((Const(D), 1))
        val k = kCtr(0)
        val rowaddr2 = Mul(rr, Const(D))
        val addr2 = Add(rowaddr2, k)
        val rval2 = Ld(bx, addr2)
        println(s"""addr2 vecWidth = ${addr2.vecWidth}""")
        println(s"""rval2 vecWidth = ${rval2.vecWidth}""")
        val subval = Mul(rval2, pipe2Res)
        println(s"""subval vecWidth = ${subval.vecWidth}""")
        val stval = St(subRAM, k, subval)
        val pipe3 = Pipe(kCtr, GraphNode(rowaddr2, addr2, rval2, subval, stval))

        // Pipe 4: Accumulator reduction
        val gradient = BRAM(T, D, shouldMetapipeline)
        val k2Ctr = Ctr(List(innerPar))((Const(D), 1))
        val k2 = k2Ctr(0)
        val v1 = Ld(gradient, k2)
        val v2 = Ld(subRAM, k2)
        val s = Add(v1, v2)
        val st = St(gradient, k2, s)
        val pipe4 = Pipe(k2Ctr, GraphNode(v1, v2, s, st))

      // Construct pipeline to process one row
      val rowPipe = if (shouldMetapipeline) MetaPipeline(rrCtr, dotPipe, pipe2, pipe3, pipe4) else Sequential(rrCtr, dotPipe, pipe2, pipe3, pipe4)
      // End }

      val gradientPipe = if (shouldMetapipeline) MetaPipeline(rCtr, offChipLoad, rowPipe) else Sequential(rCtr, offChipLoad, rowPipe)
    // End gradient }

    // Compute theta = theta + alpha * gradient {
    val alpha = ArgIn(T)("alpha")
    val pCtr = Ctr((Const(D), 1))
    val p = pCtr(0)
    val thetaVal2 = Ld(theta, p)
    val gradientVal = Ld(gradient, p)
    val times = Mul(gradientVal, alpha)
    val add = Add(thetaVal2, times)
    val stTheta = St(theta, p, add)
    val updatePipe = Pipe(pCtr, GraphNode(thetaVal2, gradientVal, times, add, stTheta))
    // End theta compute }

    val top = Sequential(gradientPipe, updatePipe)
    top
  }

  def test(args: String*) {
//    val tileSize = args(0).toInt
//    val C = 960
//
//    setArg("C",C)
//    val vec1 = Array.tabulate(C){i => util.Random.nextDouble * 100}
//    val vec2 = Array.tabulate(C){i => util.Random.nextDouble * 100}
//
//    setMem("vec1", vec1)
//    setMem("vec2", vec2)
//
//    run()
//
//    val outGold = vec1.zip(vec2).map{case (a,b) => a*b}.sum
//    val out = getArg[Double]("out")
//
//    compare(out, outGold)
  }
}


//object LogRegDSE extends DSE {
//  val name = "LogReg"
//  val makeDesign = () => new LogRegDesign{}
//
//  val TileSize = Discrete("TileSize", 96 to 19200 by 96)
//  val OuterPar = Discrete("OuterPar", 1 to 6)
//  val InnerPar = Discrete("InnerPar", 1 to 19200)
//  val Metapipe = Category("Metapipe", true, false)
//
//  (InnerPar <= TileSize) and (InnerPar divides TileSize)
//}
