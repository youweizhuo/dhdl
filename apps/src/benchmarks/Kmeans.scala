
import dhdl.graph._
import dhdl.codegen._
import dhdl.{Design, DesignTest, DSE}
import scala.math._

object Kmeans extends KmeansDesign
object KmeansTest extends KmeansDesign with DesignTest {
  def test(args: String*) {
    val ptTileSize = args(0).toInt
    val dimTileSize = args(1).toInt
    println("Do nothing!")
  }
}
trait KmeansDesign extends Design {

  // Tuning parameters
  var shouldMetapipeline = true
  var ptsLoopPar = 1
  var centroidLoopPar = 4
  var distLoopPar = 8
  var accumLoopPar = 1
  var avgLoopPar = 1

  var ptTileSize = 0
  val dimTileSize = 96

	def compNewCents(numCents:CombNode, dim:CombNode, newCentsAccum:BRAM,
		newCentsCount:BRAM) = {
	 	val newCentsB = BRAM(FloatPt(), ptTileSize*dimTileSize, false)

		val ctrs_newCents = Ctr(List(1, avgLoopPar))((numCents,1),(dim,1))
		val centIdx = ctrs_newCents(0)
		val dimIdx = ctrs_newCents(1)
		val newCentIdxMul = Mul(centIdx,dim)
		val newCentIdx = Add(newCentIdxMul,dimIdx)
		val newCentsAccumLd = Ld(newCentsAccum, newCentIdx)
		val newCentsCountLd = Ld(newCentsCount,centIdx)
		val countAsFloat = Fix2Float(newCentsCountLd)
		val newCent = Div(newCentsAccumLd,countAsFloat)
		val compNewCent = St(newCentsB, newCentIdx, newCent)
		(Pipe(ctrs_newCents,
			GraphNode(newCentIdxMul,newCentIdx,newCentsAccumLd,newCentsCountLd,newCent,compNewCent)),
			newCentsB)
	}

	def accumCloestCents(numPoints:CombNode, points:OffChipArray, dim:CombNode,
		numCents:CombNode, cents:BRAM) = {
    val tileCtr = Ctr((numPoints,ptTileSize))
    val ptsB = BRAM(FloatPt(),ptTileSize*dimTileSize, false)
    val ptsLd = TileMemLd(points, dim, tileCtr(0), Const(0), ptsB, ptTileSize, dimTileSize)
		/* { --------- */
    val ptsCtr = Ctr(List(ptsLoopPar))((Const(ptTileSize),1))
		val (meta_closestCent, closestCentIdx) =
			findMinCent(ptsCtr, numCents, dim, ptsB, cents)
	 	val (pipe_accumClosestCent, newCentsAccum) = accumClosestCent(dim, closestCentIdx, ptsB, ptsCtr)
		val (pipe_increClosestCentCount, newCentsCount) = increCentCount(closestCentIdx)
		val meta_accumCloestCents =
			MetaPipeline(ptsCtr, meta_closestCent, Parallel(pipe_accumClosestCent,
			pipe_increClosestCentCount))
		/* } --------- */

		(MetaPipeline(tileCtr, ptsLd, meta_accumCloestCents),
			newCentsAccum,
			newCentsCount)
	}

	def accumClosestCent(dim:CombNode, closestCentIdx: CombNode, ptsB:BRAM, ptsCtr:Ctr) = {
    val newCentsAccum = BRAM(FloatPt(),ptTileSize*dimTileSize,true)
		newCentsAccum.setAccum()

		val dimCtr = Ctr(List(accumLoopPar))((dim, 1))
		val ptsAddrMul = Mul(ptsCtr(0), dim)
	  val ptsAddr = Add(ptsAddrMul, dimCtr(0))
		val cCentAddrMul = Mul(closestCentIdx, dim)
		val cCentAddr = Add(cCentAddrMul, dimCtr(0))
		val currCentSum = Ld(newCentsAccum, cCentAddr)
		val pt = Ld(ptsB, ptsAddr)
		val sumCent = Add(currCentSum, pt)
	  val accumCent = St(newCentsAccum, cCentAddr, sumCent)
		val pipe_accumClosestCent =
			Pipe(dimCtr,
				GraphNode(ptsAddrMul,ptsAddr,cCentAddrMul,cCentAddr,currCentSum,sumCent,accumCent))
		(pipe_accumClosestCent, newCentsAccum)
	}

	def increCentCount(closestCentIdx:CombNode) = {
    val newCentsCount = BRAM(UInt(),ptTileSize,true)
    newCentsCount.setAccum()

		val currCentCount = Ld(newCentsCount, closestCentIdx)
		val increCentCount = Add(currCentCount, Const(1))
		val increCentCountSt = St(newCentsCount, closestCentIdx, increCentCount)
		val pipe_increClosestCentCount = Pipe(currCentCount,increCentCount,increCentCountSt)
		(pipe_increClosestCentCount, newCentsCount)
	}

	def findMinCent(ptsCtr:Ctr, numCents:CombNode, dim:CombNode, ptsB:BRAM, cents:BRAM) = {

	 	val centCtr = Ctr(List(centroidLoopPar))((numCents, 1))

//	 	val (pipe_compDist,dist) = compDist(dim, ptsCtr(0), centCtr(0), ptsB, cents)
  val (pipe_compDist,dist) = replicates(centroidLoopPar) { i =>
    compDist(dim, ptsCtr(0), centCtr(0)(i), ptsB, cents)
  }

    //    val currMinDist = Reg(FloatPt(), false, Some(Const(100000000000000f)))

    // Raghu: This should ideally be a reducePipe
//		val pipe_compMinDist = {
//			val isCloserToPoint = Lt(dist, currMinDist)
//			val currMinDistMux = Mux(isCloserToPoint, dist, currMinDist)
//			currMinDist.write(currMinDistMux)
//			val closestCentIdxMux = Mux(isCloserToPoint, centCtr(0), closestCentIdx)
//			closestCentIdx.write(closestCentIdxMux)
//			Pipe(isCloserToPoint, currMinDistMux, closestCentIdxMux)
//		}

//    val currMinDist = Reg(FloatPt(), false, Some(Const(100000000000000f)))
//    val closestCentIdx = Reg(UInt(),true)
    val closestCentPair = Reg(CompositeType(List(FloatPt(), UInt())),true)
    val pipe_compMinDist = reducePipe(centroidLoopPar, Ctr((Const(1),1)), closestCentPair) { i =>
      val d = dist(i)(0)
      val slice = centCtr(0)(i)
//      val node = Bundle(dist(i)(0), centCtr(0)(i))
      val node = Bundle(d, slice)
      //println(s"Creating node $node with $d, $slice")
      node
    } { (a,b) => MinWithMetadata(a, b) }

		val meta_closestCent = MetaPipeline(centCtr, pipe_compDist, pipe_compMinDist)
//		currMinDist.setParent(meta_closestCent)
//		closestCentIdx.setParent(meta_closestCent)
		closestCentPair.setParent(meta_closestCent)
//		(meta_closestCent, closestCentIdx)
		(meta_closestCent, closestCentPair.get(1))
	}

	def compDist(dim:CombNode, pAIdx:CombNode, pBIdx: CombNode, pAs_td: BRAM, pBs_td:BRAM) = {
	 	val dimCtr = Ctr(List(distLoopPar))((dim, 1))
		val pAdimIdxMul = Mul(pAIdx,dim)
    val pAdimIdx = Add(pAdimIdxMul, dimCtr(0))
		val pBdimIdxMul = Mul(pBIdx,dim)
    val pBdimIdx = Add(pBdimIdxMul, dimCtr(0))
    val distAccum = Reg(FloatPt())
    val pA_1d = Ld(pAs_td, pAdimIdx)
    val pB_1d = Ld(pBs_td, pBdimIdx)
    val diff = Sub(pA_1d, pB_1d)
    val sqrDiff = Mul(diff, diff)
    val sumSqrDiff = Add(distAccum, sqrDiff)
    distAccum.write(sumSqrDiff)

		val pipe = Pipe(dimCtr,
			GraphNode(pAdimIdxMul, pBdimIdxMul, pAdimIdx, pBdimIdx, pA_1d, pB_1d, diff, sqrDiff),
			ReduceTree(sqrDiff, distAccum, sumSqrDiff))
		distAccum.setParent(pipe)
		(pipe, List(distAccum))
	}

  def main(args: String*) = {
    if (args.size != 8) {
      println("\nUsage: Kmeans <#pointTileSize> <#ptsLoopPar> <#centroidLoopPar> <#distLoopPar> <#accumLoopPar> <#avgLoopPar> <shouldMetapipeline> <dimSize>")
      sys.exit(0)
    }
    ptTileSize = args(0).toInt
    //dimTileSize = args(1).toInt
    ptsLoopPar = args(1).toInt
    centroidLoopPar = args(2).toInt
    distLoopPar = args(3).toInt
    accumLoopPar = args(4).toInt
    avgLoopPar = args(5).toInt
    shouldMetapipeline = args(6) == "true"

    /* Algorithm Inputs */
    val numPoints = ArgIn("numPoints"); bound(numPoints, 960000)
    val numCents = ArgIn("numCents");   bound(numCents, 16)
    val dim = ArgIn("dimension");       bound(dim, 96)

    val points = OffChipArray(FloatPt())("points", numPoints, dim)
    val cents = BRAM(FloatPt(),ptTileSize*dimTileSize, false)
    val centsLd = TileMemLd(points, dim, Const(0), Const(0), cents, ptTileSize, dimTileSize)
		val (meta_accumCents, newCentsAccum, newCentsCount) =
			accumCloestCents(numPoints, points, dim, numCents, cents)
		val (pipe_newCents, newCentsB) = compNewCents(numCents, dim, newCentsAccum, newCentsCount)
		val newCents =
			OffChipArray(FloatPt())("newCents", numCents, dim)
	 	val newCentsSt = TileMemSt(newCents, dim, Const(0), Const(0), newCentsB, ptTileSize, dimTileSize)
    val top = Sequential(centsLd, meta_accumCents, pipe_newCents, newCentsSt)
    top
  }
}

object KmeansDSE extends DSE {
  val name = "Kmeans"
  val makeDesign = () => new KmeansDesign{}
  //override val verbose = true
  val PtTileSize = Discrete("PtTileSize", (10 to 960 by 10).toList :+ 1)  // 96
  val PtLoopPar = Discrete("PtLoopPar", List(1))                          // 1
  val CtLoopPar = Discrete("CtLoopPar", 1 to 16)                          // 16
  val DstLoopPar = Discrete("DstLoopPar", 1 to 96)                        // 12
  val AccLoopPar = Discrete("AccLoopPar", 1 to 96)                        // 12
  val AvgLoopPar = Discrete("AvgLoopPar", 1 to 96)                        // 12
  val Metapipe = Category("Metapipe", true, false)                        // 2
  val DimSize = Discrete("DimSize", List(96)) // Not actually a parameter, just used to narrow space

  (PtLoopPar <= PtTileSize) and (PtLoopPar divides PtTileSize)
  (DstLoopPar divides DimSize)
  (AccLoopPar divides DimSize)
  (AvgLoopPar divides DimSize)
}