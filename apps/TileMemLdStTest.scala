import dhdl.graph._
import dhdl.codegen._
import dhdl.codegen.dot.GraphvizCodegen
import dhdl.analysis.IRPrintPass
import dhdl.DesignTest

import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer

object TileMemLdStTest extends DesignTest {

  def stageLists(x: List[List[CtrlNode]]) = {
    val numStages = x(0).size
    val stages = (0 until numStages).map { i =>
      val ctrlnodes = x.map{_(i)}
      Parallel(ctrlnodes:_*)
    }
    stages
  }

  def main(args: String*) = {
    if (args.size != 1) {
      println("\nUsage: TileMemLdStTest <vecTileSize>")
      sys.exit(0)
    }
    val vecSize = ArgIn("vecSize")
    val tileSize = args(0).toInt


    // Off-chip memory input
    val vec1 = OffChipArray("vecIn1", vecSize)

    // Off-chip memory output
    val vecOut = OffChipArray("vecOut", vecSize)

    // How much to unroll
    val par = 6
    val seqCtr = Ctr(List(par))((vecSize,tileSize))

    val s1 = replicate(par) { i =>
      val tileRAM1 = BRAM(tileSize, true)
      val tileLd1 = TileMemLd(vec1, vecSize, Const(0), seqCtr(0)(i), tileRAM1, 1, tileSize)
      (tileLd1, List(tileRAM1))
    }

    val accum = BRAM(tileSize, true)
    val s2 = reduceAll(s1._2.flatten, accum) { (a,b,last) =>
      val res = if (last) accum else BRAM(tileSize, true)
      val ctr = Ctr((Const(tileSize), 1))
      val data1 = Ld(a, ctr(0))
      val data2 = Ld(b, ctr(0))
      val add1 = Add(data1, data2)
      val st = St(res, ctr(0), add1)
      val pipe = Pipe(ctr, GraphNode(data1, data2, add1, st))
      (pipe, res)
    }

    val s3 = TileMemSt(vecOut, vecSize, Const(0), seqCtr(0)(0), s2._2, 1, tileSize)

    val body = List(s1._1) ++ s2._1 ++ List(s3)
    val top = MetaPipeline(seqCtr, body:_*)
    top
  }

  def test(args: String*) {
    val tileSize = args(0.toInt)
    setArg("vecSize", 192)
    val data = Array.tabulate(192){i => i}
    setMem("vecIn", data)

    run()

    val out = getMem[Int]("vecIn", 0 until 192)
    compare(out, data)
  }
}


