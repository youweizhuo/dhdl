package dhdl.transform
import dhdl.graph._
import dhdl.{Config,Design}

import scala.collection.mutable.HashMap
import scala.collection.mutable.Set
import dhdl.codegen.dot.GraphvizCodegen

abstract class Transformer(implicit design: Design) {
  protected val debug = false
  var isInit = false

  protected val transformed = HashMap[Node,Node]()
  val allowReps = Set[Node]()

  def msg(x: String) = if (Config.dse) () else System.out.println(x)
  def dbg(x: String) = if (Config.dse) () else if (debug) System.out.println(x)

  def reset() {
    transformed.clear()
    allowReps.clear()
    isInit = false
  }

  val name = "undefined"
  def run(origNode: Node): Node = {
    if (!Config.quick) {
      val beforeGraph = new GraphvizCodegen(s"before_${name}")
      beforeGraph.run(origNode)
    }

    initTransform()
    val transformedNode = transformNode(origNode)
    finTransform()

    if (!Config.quick) {
      val afterGraph = new GraphvizCodegen(s"after_${name}")
      afterGraph.run(transformedNode)
    }

    transformedNode
  }

  def initTransform() : Unit = {
    isInit = true
  }

  def transformNode(node: Node) : Node = {
    dbg(s"""Transforming node $node..""")
    if (transformed.contains(node) && !allowReps.contains(node)) {
      val t = transformed(node)
      t
    } else {
      design.archiveNode(node)
      val tnode = node match {
        case n@Fix2Float(op, t) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Fix2Float(top, t)
        case n@Float2Fix(op, t) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Float2Fix(top, t)
        case n@Not(op) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Not(top)
        case n@Add(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node) && !allowReps.contains(node)) transformed(node) else Add(top1, top2)
        case n@Sub(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node) && !allowReps.contains(node)) transformed(node) else Sub(top1, top2)
        case n@Mul(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Mul(top1, top2)
        case n@Div(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Div(top1, top2)
        case n@Lt(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node)) transformed(node) else Lt(top1, top2)
        case n@Gt(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Gt(top1, top2)
        case n@Mux(sel, i1, i2) =>
          val tsel = transformNode(sel).asInstanceOf[CombNode]
          val ti1 = transformNode(i1).asInstanceOf[CombNode]
          val ti2 = transformNode(i2).asInstanceOf[CombNode]
          if (transformed.contains(node)) transformed(node) else Mux(tsel, ti1, ti2)
        case n@Bundle(elems @_*) =>
          val telems = elems.map { transformNode(_).asInstanceOf[CombNode] }
          Bundle(telems:_*)
        case n@MinWithMetadata(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node)) transformed(node) else MinWithMetadata(top1, top2)
        case n@MaxWithMetadata(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node)) transformed(node) else MaxWithMetadata(top1, top2)
        case n@Max(op1, op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          if (transformed.contains(node)) transformed(node) else Max(top1, top2)
        case n@Eq(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Eq(top1, top2)
        case n@Le(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Le(top1, top2)
        case n@Ge(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Ge(top1, top2)
        case n@And(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          And(top1, top2)
        case n@Or(op1,op2) =>
          val top1 = transformNode(op1).asInstanceOf[CombNode]
          val top2 = transformNode(op2).asInstanceOf[CombNode]
          Or(top1, top2)
        case n@Abs(op) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Abs(top)
        case n@Exp(op) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Exp(top)
        case n@Log(op) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Log(top)
        case n@Sqrt(op) =>
          val top = transformNode(op).asInstanceOf[CombNode]
          Sqrt(top)
       case n@ArgIn(id) =>
          n
        case n@ArgOut(id,value) =>
          val tvalue = transformNode(value).asInstanceOf[CombNode]
          ArgOut(id,tvalue)
        case n@Ld(mem, addr, stride) =>
          val tcomposites = n.compositeValues.map { transformNode(_).asInstanceOf[Wire] }
          val tmem = transformNode(mem).asInstanceOf[MemNode]
          val taddr = transformNode(addr).asInstanceOf[CombNode]
          val newld = new Ld(tmem, taddr, stride) { override val compositeValues = tcomposites }
          newld
        case n@St(mem, addr, data, start, stride) =>
          val tmem = transformNode(mem).asInstanceOf[MemNode]
          val taddr = transformNode(addr).asInstanceOf[CombNode]
          val tdata = transformNode(data).asInstanceOf[CombNode]
          St(tmem, taddr, tdata, start, stride)
        case n@Const(value) =>
          n
        case n@Slice(in, from, to) =>
          val tin = transformNode(in).asInstanceOf[CombNode]
          Slice(tin, from, to)
        case n:Ctr =>
          val maxNStride = n.max.zip(n.stride)
          val tmaxs = maxNStride.map { t =>
            (transformNode(t._1).asInstanceOf[CombNode], t._2)
          }
          val newctr = Ctr(n.par)(tmaxs:_*)
          newctr.ctrs.zip(n.ctrs).map { t =>
            val newctr = t._1
            val oldctr = t._2
            transformed += oldctr -> newctr
            transformed += newctr -> newctr
          }
          newctr
        case n@GraphNode(nodes @ _*) =>
          val newnodes = n.nodes.toList.map { transformNode(_).asInstanceOf[CombNode] }
          GraphNode(newnodes:_*)
        case n@ReduceTree(oldval, accum, graph @_*) =>
          val toldval = transformNode(oldval).asInstanceOf[CombNode]
          val taccum = transformNode(accum).asInstanceOf[Reg]
          val tgraph = graph.map { g => transformNode(g).asInstanceOf[GraphNode] }
          ReduceTree(toldval, taccum, tgraph:_*)
        case n@CollectReduceTree(in, accum, body @_*) =>
          val tin = in.map { transformNode(_).asInstanceOf[BRAM] }
          val taccum = transformNode(accum).asInstanceOf[BRAM]
          val tbody = body.map { b => transformNode(b).asInstanceOf[CtrlNode] }
          CollectReduceTree(tin, taccum, tbody:_*)
        case n@Pipe(ctr, mapNode, redNode) =>
          val tctr = transformNode(ctr).asInstanceOf[Ctr]
          val tmap = transformNode(mapNode).asInstanceOf[GraphNode]
          if (n.hasReduce) {
            val tred = transformNode(redNode.get).asInstanceOf[ReduceTree]
            Pipe(tctr, tmap, tred)
          } else {
            Pipe(tctr, tmap)
          }
        case n@Parallel(nodes @ _*) =>
          val newnodes = n.nodes.map { transformNode(_).asInstanceOf[CtrlNode] }
          Parallel(newnodes:_*)
        case n: TileMemLd =>
          val tbase = transformNode(n.baseAddr).asInstanceOf[OffChipArray]
          val tcoldim = transformNode(n.colDim).asInstanceOf[CombNode]
          val tidx0 = transformNode(n.idx0).asInstanceOf[CombNode]
          val tidx1 = transformNode(n.idx1).asInstanceOf[CombNode]
          val tbuf = n.buf.map { transformNode(_).asInstanceOf[MemNode] }
          val tdim0 = n.tileDim0
          val tdim1 = n.tileDim1
          val tforce = transformNode(n.force).asInstanceOf[CombNode]
          val newnode = TileMemLd(tbase, tcoldim, tidx0, tidx1, tbuf, tdim0, tdim1).withForce(tforce)
          transformed += n.isLoading -> newnode.isLoading
          transformed += newnode.isLoading -> newnode.isLoading
          newnode
        case n: TileMemSt =>
          val tbase = transformNode(n.baseAddr).asInstanceOf[OffChipArray]
          val tcoldim = transformNode(n.colDim).asInstanceOf[CombNode]
          val tidx0 = transformNode(n.idx0).asInstanceOf[CombNode]
          val tidx1 = transformNode(n.idx1).asInstanceOf[CombNode]
          val tbuf = transformNode(n.buf).asInstanceOf[MemNode]
          val tdim0 = n.tileDim0
          val tdim1 = n.tileDim1
          val tforce = transformNode(n.force).asInstanceOf[CombNode]
          val newnode = TileMemSt(tbase, tcoldim, tidx0, tidx1, tbuf, tdim0, tdim1).withForce(tforce)
          transformed += n.isStoring -> newnode.isStoring
          transformed += newnode.isStoring -> newnode.isStoring
          newnode
        case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
          val tctr = transformNode(ctr).asInstanceOf[Ctr]
          val tregChain = transformNode(regChain).asInstanceOf[GraphNode]
          val newnodes = nodes.map { transformNode(_).asInstanceOf[CtrlNode] }
          val tnumIter = transformNode(numIter).asInstanceOf[GraphNode]
          msg(s"""New MetaPipeline: $tctr, $tregChain, $tnumIter, $newnodes""")
          new MetaPipeline(tctr, tregChain, tnumIter, newnodes:_*)
        case n@Sequential(ctr, numIter, nodes @ _*) =>
          val tctr = transformNode(ctr).asInstanceOf[Ctr]
          val newnodes = n.nodes.map { transformNode(_).asInstanceOf[CtrlNode] }
          val tnumIter = transformNode(numIter).asInstanceOf[GraphNode]
          Sequential(tctr, tnumIter, newnodes:_*)
        case n@BRAM(depth, isDblBuf) =>
          val newn = BRAM(n.t, depth, isDblBuf)
          newn.banks = n.banks
          newn
        case n@PQ(depth, isDblBuf) =>
          val newn = PQ(n.t, depth, isDblBuf)
          newn.banks = n.banks
          newn
        case n@Reg(isDblBuf,init) =>
          val treg = Reg(n.vecWidth, n.t, isDblBuf, init)
          transformed += node -> treg
          transformed += treg -> treg
          (0 until n.compositeValues.size).map { i =>
            transformed += n.get(i) -> treg.get(i)
            transformed += treg.get(i) -> treg.get(i)
          }
          val tin = transformNode(n.input).asInstanceOf[CombNode]
          treg.write(tin)
          treg.setParent(n.getParent())
          if (isDblBuf) {
            n.getReaders.map { r =>
              treg.addReader(r)
            }
            if (!n.getWriter.isEmpty) {
              treg.setWriter(n.getWriter.get)
            }
          }

          treg
        case n@DummyCtrlNode() =>
          n
        case n: DummyMemNode =>
          n
        case n: Wire =>
          val tp = n.t
          if (transformed.contains(node)) transformed(node) else Wire(tp)
        case n@OffChipArray(id, sizes @ _*) =>
          val newsizes = sizes.map(transformNode(_).asInstanceOf[ConstValueNode])
          OffChipArray(n.t)(id, newsizes:_*)
        case n@PE(deps @ _*) =>
          val newdeps = deps.map(transformNode(_).asInstanceOf[PE])
          PE(newdeps:_*)
        case _ =>
          throw new Exception(s"Don't know how to transform $node")
      }

      if (transformed.contains(node)) {
        design.archiveNode(transformed(node))
      }
      transformed += node -> tnode
      transformed += tnode -> tnode
      design.addNode(tnode)
      if (debug) {
        msg(s"""Returning node $tnode (transform $node)""")
      }
      tnode
    }
  }

  def finTransform() : Unit = {
    if (debug) {
      msg(s"""In finTransform""")
    }
    // Needed to handle parent info. Register's parent might not
    // have transformed yet. So add this information once all nodes
    // have been transformed
    val allRegs = design.allNodes.filter(_.isInstanceOf[Reg]).toList.asInstanceOf[List[Reg]]
    allRegs.foreach { reg =>
      reg.setParent(transformed(reg.getParent()).asInstanceOf[CtrlNode])
      if (reg.isDblBuf) {
        if (debug) {
          msg(s"""reg.readers = ${reg.getReaders}""")
        }
        val oldreaders = reg.getReaders.clone
        reg.clearReaders
        oldreaders.map { r =>
          reg.addReader(transformed(r).asInstanceOf[CtrlNode])
        }
        if (!reg.getWriter.isEmpty) {
          if (debug) {
            msg(s"""reg.writer = ${reg.getWriter.get}""")
          }
          reg.setWriter(transformed(reg.getWriter.get).asInstanceOf[CtrlNode])
        }
      }
      if (debug) {
        msg(s"""Finish finTransform""")
      }
    }

    val allArgOuts = design.allNodes.filter(_.isInstanceOf[ArgOut]).toList.asInstanceOf[List[ArgOut]]
    allArgOuts.map { argOut =>
      if (argOut.getParent() != null) {
        transformNode(argOut).setParent(transformed(argOut.getParent()).asInstanceOf[CtrlNode])
      }
    }

    // Transform magnitude annotations
    for ((arg,bnd) <- design.nodeBounds) { design.nodeBounds += transformed(arg).asInstanceOf[CombNode] -> bnd }
  }
}
