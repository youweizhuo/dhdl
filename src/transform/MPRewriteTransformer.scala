package dhdl.transform
import dhdl.graph._
import dhdl.Design

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer

class MPRewriteTransformer(implicit design: Design) extends Transformer {
  override val name = "MPRewriter"

  def addRegChain(ctrWire: Wire, mp: MetaPipeline, nodeSubstMap: HashMap[CtrlNode, HashMap[Node, Node]], nodeWriteMap: HashMap[CtrlNode, ListBuffer[Reg]]) = {
    val regLB = ListBuffer[Reg]()
    nodeSubstMap(mp.nodes(0)) += ctrWire -> ctrWire
    (1 until mp.nodes.size).map { i =>
      val s = mp.nodes(i)
      val r = Reg(ctrWire.vecWidth, true)
      if (regLB.isEmpty) r.write(ctrWire) else r.write(regLB.last)
      r.setParent(s)
      mp.regChain.add(r)
      regLB.append(r)
      nodeSubstMap(s) += ctrWire -> r
      nodeWriteMap(mp.nodes(i-1)) += r
      mp.nodes.map { nodeSubstMap(_) += r -> r }
    }
    regLB.toList
  }

  override def transformNode(node: Node) = {
    if (transformed.contains(node)) {
      transformed(node)
    } else {
      val tnode = node match {
        case m@MetaPipeline(ctr, regChain, numIter, nodes @ _*)  =>
          transformed += ctr -> ctr
          val nodeSubstMap = HashMap[CtrlNode, HashMap[Node, Node]]()
          val nodeWriteMap = HashMap[CtrlNode, ListBuffer[Reg]]()

          nodes.map { s =>
            nodeSubstMap += s -> HashMap[Node, Node]()
            nodeWriteMap += s -> ListBuffer[Reg]()
          }

          // Establish substs for each node
          ctr.ctrs.map { addRegChain(_, m, nodeSubstMap, nodeWriteMap) }

          // For each stage s:
          val readerRegs = ListBuffer[Reg]()
          val newnodes = nodes.map { s =>
            // 1. Add substitutions in nodeSubstMap(s) to transformed
            val substs = nodeSubstMap(s)
            transformed ++= substs

            // 2. Call transformNode(s)
            val newstage = transformNode(s).asInstanceOf[CtrlNode]

            // 3. Remove substitutions in nodeSubstMap(s)
            transformed --= substs.keys

            // 4. Set 'newstage' as the parent of all regs involved here
            readerRegs.map { r =>
              r.clearReaders()
              r.addReader(newstage)
            }
            readerRegs.clear
            nodeWriteMap(s).map { r =>
              r.setWriter(newstage)
              readerRegs.append(r)
            }
            newstage
          }
          val newnode = new MetaPipeline(ctr, regChain, numIter, newnodes:_*)
          msg(s"""Transforming $node -> $newnode""")
          newnode
        case _ =>
          super.transformNode(node)
      }

      // [HACK] This part is duplicated with the parent, which is bad design
      if (transformed.contains(node)) {
        design.archiveNode(transformed(node))
      }
      transformed += node -> tnode
      transformed += tnode -> tnode
      design.addNode(tnode)
      tnode
    }
  }
}
