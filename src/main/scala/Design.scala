package dhdl

import graph._

import analysis.IRPrintPass
import transform._
import analysis._

import codegen._
import codegen.dot._
import codegen.maxj._
import codegen.scalagen._

import scala.collection.mutable.Queue
import scala.collection.mutable.ListBuffer
import java.nio.file.{Paths, Files}
import scala.io.Source

import scala.collection.mutable.{Set,Map}

trait Design { self =>

  implicit val design: Design = self

  private var argInId = 0
  private var argOutId = 0
  private var constId = 0
  def nextArgInId = {argInId += 1; argInId}
  def nextArgOutId = {argOutId += 1; argOutId}
  def nextConstId = {constId += 1; constId}

  def nextId(x: Any) = x match {
    case x: ArgIn => nextArgInId
    case x: ArgOut => nextArgOutId
    case x: Const[_] => nextConstId
  }

  private var nextSym = 0
  def nextId = {nextSym += 1; nextSym }

  private var _checkIds = true
  def checkIds = _checkIds && !Config.quick

  var area = Resources()
  var cycles = 0.0
  var stage = 0

  var lutRoutingModel: LUTRoutingModel = null //new LUTRoutingModel()
  var regFanoutModel: RegFanoutModel = null //new RegFanoutModel()
  var unavailALMModel: UnavailALMModel = null //new UnavailALMModel()
  //var bramModel: BRAMDupModel = null
  var areaAnalysis: AreaAnalysis = null //new AreaAnalysis(lutRoutingModel, regFanoutModel, unavailALMModel)

  val archivedNodes = Set[Node]()
  val nodeBounds = Map[CombNode, Double]()
  val argIds = Set[String]()
  val memIds = Set[String]()
  val allNodes = Set[Node]()
  val mpRewriteTransformer = new MPRewriteTransformer()
  val unroller = new UnrollRedTreeTransformer()
  val mpAnalysis = new MPDblBufAnalysis()
  val boundsAnalysis = new BoundsAnalysis()
  val cycleAnalysis = new CycleAnalysis()
  val contentionAnalysis = new ContentionAnalysis()


  def reset() {
    allNodes.clear()
    argIds.clear()
    memIds.clear()
    archivedNodes.clear()
    nodeBounds.clear()
    mpRewriteTransformer.reset()
    unroller.reset()
    mpAnalysis.reset()
    boundsAnalysis.reset()
    cycleAnalysis.reset()
    areaAnalysis.reset()
    contentionAnalysis.reset()
    _checkIds = true
    nextSym = 0
    area = Resources()
    cycles = 0.0
    stage = 0
  }

  def addNode(n: Node) { allNodes += n }
  def archiveNode(n: Node) {
    allNodes -= n
    archivedNodes += n
  }

  def consts = allNodes.filter{_.isInstanceOf[Const[_]]}.asInstanceOf[Set[Const[_]]]

  def msg(x: String) = if (Config.dse) () else System.out.println(x)


  var first = true

  def processTop(top: Node): Node = {
    _checkIds = false // Disable duplicate ID checking prior to transformers

    if (first) {
      if (lutRoutingModel eq null) lutRoutingModel = new LUTRoutingModel()
      if (regFanoutModel eq null) regFanoutModel = new RegFanoutModel()
      if (unavailALMModel eq null) unavailALMModel = new UnavailALMModel()
      //if (bramModel eq null) bramModel = new BRAMDupModel()
      if (areaAnalysis eq null) areaAnalysis = new AreaAnalysis(lutRoutingModel, regFanoutModel, unavailALMModel)
      first = false
    }

    //val flatten = new CollectTreeFlattenTransformer()
    //val t0 = flatten.run(top)
    val t0 = top
    val t1 = if (Config.quick) t0 else { stage = 1; mpRewriteTransformer.run(t0) }
    val t2 = if (Config.quick) t1 else { stage = 2; unroller.run(t1) }

    if (Config.genMaxJ) mpAnalysis.run(t2)   // Set readers and writers of double buffers (technically a transformer)

    boundsAnalysis.analyze(t2)

    // Check if number of memory streams is less than maximum
    val numTileLdSts = allNodes.count(x => x.isInstanceOf[TileMemLd] || x.isInstanceOf[TileMemSt])
    if (numTileLdSts > 13) {
      // MaxJ reports 15 is the max, but seems the max of tile loads is 13
      msg("WARNING: Invalid design: Total number of tile load and store units must be less than 14")
      cycles = -1.0
    }
    else {
      contentionAnalysis.analyze(t2)
      cycles = cycleAnalysis.analyze(t2)
    }

    area = areaAnalysis.analyze(t2)

    msg(s"Estimated cycles: ${cycles}")
    msg(s"Estimated runtime (at 150MHz): " + "%.8f".format(cycles/150000000) + "s")
    msg("Estimated area:")
    msg(s"ALMs: ${area.alms} / 262400 (" + "%.2f".format(100*area.alms.toDouble/262400) + "%)")
    msg(s"Regs: ${area.regs}")
    msg(s"DSPs: ${area.dsps} / 1963 (" + "%.2f".format(100*area.dsps.toDouble/1963) + "%)")
    msg(s"BRAMs: ${area.brams} / 2,567 (" + "%.2f".format(100*area.brams.toDouble/2567) + "%)")

    t2
  }

  def reduceAll(nodes: List[MemNode], accum: MemNode)(body: (MemNode, MemNode, Boolean) => (CtrlNode, MemNode)) = {
    // -- Version 1
    /*var inputs = nodes
    val parallelList = ListBuffer[CtrlNode]()
    while (inputs.length > 1) {
      val layer = List.tabulate(inputs.length / 2){i => body( inputs(2*i), inputs(2*i+1), false)}
      val ctrls = layer.map(_._1)
      inputs = if (inputs.length % 2 == 0) layer.map(_._2) else layer.map(_._2) :+ inputs.last
      parallelList += (if (ctrls.length > 1) Parallel(ctrls:_*) else ctrls(0))
    }
    val (ctrl,mem) = body(inputs.head, accum, true)
    parallelList += ctrl
    (parallelList, mem)*/

    // -- Version 2
    val inputs = Queue[MemNode]()
    nodes.map { inputs += _ }
    inputs += DummyMemNode()

    val curNodeList = ListBuffer[CtrlNode]()
    val parallelList = ListBuffer[CtrlNode]()
    while (inputs.size > 2) {
      val i1 = inputs.dequeue
      val i2 = inputs.dequeue
      val out = body(i1, i2, false)
      curNodeList += out._1
      inputs += out._2
      inputs.head match {
        case _: DummyMemNode =>
          val n = inputs.dequeue
          inputs += n
          val stages = curNodeList.toList
          val pnode = if (stages.size > 1) Parallel(stages:_*) else stages(0)
          parallelList += pnode
          curNodeList.clear
        case _ =>
          // Do nothing
      }
    }

    // Last node
    val i1 = inputs.dequeue
    val i2 = accum
    val out = body(i1, i2, true)
    parallelList += out._1

    (parallelList, out._2)
  }

  def treeReduce(x: List[CombNode], rF: (CombNode, CombNode) => CombNode): CombNode = {
    if (x.length == 1) x.head
    else if (x.length % 2 == 0)
      treeReduce(List.tabulate(x.length / 2){i => rF(x(2*i), x(2*i + 1)) }, rF)
    else
      treeReduce(List.tabulate(x.length / 2){i => rF(x(2*i), x(2*i + 1)) } :+ x.last, rF)
  }

  /** Replicate a Pipe with a map and reduce such that the reduces of the replicated pipes are fused together.
   *  This function does the replicate and fuse in one step. This is used in apps like TPC-HQ6 where the
   *  results from Pipe replication must be reduced again to produce the final result.
   *  @param par: Amount by which this pipe must be replicated.
   *  @param ctr: Common counter shared by the big fused body
   *  @param accum: Accumulator
   *  @param mapFun: Map function defined as a func over a 'parallel id' integer that returns a CombNode
   *  @param redFun: Reduce function
   */
  def reducePipe(par: Int, ctr: Ctr, accum: Reg)(mapFun: Int => CombNode)(redFun: (CombNode, CombNode) => CombNode) = {
    var mapOut: CombNode = null
    val newMap = Block {
      val mapNodes = (0 until par).map{i => mapFun(i) }
      mapOut  = treeReduce(mapNodes.toList, redFun)
    }

    /*
    val mapNodes = (0 until par).map { i => Block(mapFun(i)) }
    val mapOuts = mapNodes.map { _.getouts()(0).asInstanceOf[CombNode] }

    val newMap = GraphNode()
    mapNodes.map { g => newMap.nodes ++= g.nodes }

    val treeNodesGraph = connectAsTree(mapOuts.toList, redFun)
    treeNodesGraph.nodes.map { newMap.add(_) }

    val mapOut = treeNodesGraph.getouts()(0).asInstanceOf[CombNode]*/

    val writerNode = redFun(mapOut, accum)
    accum.write(writerNode)
    val redtree = ReduceTree(mapOut, accum, writerNode)
    Pipe(ctr, newMap, redtree)
  }

  /** Unroll-and-fuse reduce functions that are collections
   */
  def reducePipeAccum(par: Int, ctr: Ctr, accum: MemNode)(mapFun: Int => CombNode)(redFun: (CombNode, CombNode) => CombNode) = {
    // Version 1
    var mapOut: CombNode = null
    val newMap = Block {
      val mapNodes = (0 until par).map{i => mapFun(i)}
      mapOut = treeReduce(mapNodes.toList, redFun)
    }

    // Original version
    /*val mapNodes = (0 until par).map { i => Block(mapFun(i)) }
    val mapOuts = mapNodes.map { _.getouts()(0).asInstanceOf[CombNode] }

    val newMap = GraphNode()
    mapNodes.map { g => newMap.nodes ++= g.nodes }

    val treeNodesGraph = treeReduce(mapOuts.toList, redFun) //connectAsTree(mapOuts.toList, redFun)
    treeNodesGraph.nodes.map { newMap.add(_) }

    val mapOut = treeNodesGraph.getouts()(0).asInstanceOf[CombNode]*/

    val oldval = Ld(accum, ctr(0))
    val newval = redFun(mapOut, oldval)
    val writerNode = St(accum, ctr(0), newval)
    newMap.add(oldval, newval, writerNode)

    Pipe(ctr, newMap)
  }


  def replicate(x: Int)(body: Int => (CtrlNode, List[MemNode])) = {
    val rawbody = (0 until x).map{i => body(i) }
    val ctrlNodes = rawbody.map{_._1}
    val memNodes = rawbody.map{_._2}.toList
    (Parallel(ctrlNodes:_*), memNodes)
  }

  def replicates(x: Int)(body: Int => (CtrlNode, List[Reg])) = {
    val rawbody = (0 until x).map{i => body(i) }
    val ctrlNodes = rawbody.map{_._1}
    val regs = rawbody.map{_._2}.toList
    (Parallel(ctrlNodes:_*), regs)
  }


  //def irprint(n: Node) { new IRPrintPass().run(n) }
  //def irprintGraph(n: Node) { new GraphvizCodegen(s"$n").run(n) }

  // Set bound as 10^x
  def magnitude(arg: ArgIn, mag: Int) { nodeBounds += (arg -> Math.pow(10,mag)) }
  // Set upper bound of argument
  def bound(arg: ArgIn, bnd: Double) { nodeBounds += (arg -> bnd) }

  def main(args: String*): Node
  def main(args: Array[String]): Unit = {
    msg(args.mkString(", "))
    val top = main(args:_*)

    if (Config.genDot) {
      val origGraph = new GraphvizCodegen(s"orig")
      origGraph.run(top)
    }

    val transformedTop = processTop(top)

    if (Config.genMaxJ) {
      if (Config.genDot) {
        msg("Generating dot graph")
        val dot = new GraphvizCodegen()
        dot.run(transformedTop)
      }
      if (Config.genMaxJ) {
        msg("Generating MaxJ")
        val maxj = new MaxJCodegen()
        maxj.run(transformedTop)
      }
    }
  }

}
