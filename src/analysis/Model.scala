package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._

case class Resources (
  alms: Int = 0,
  regs: Int = 0,
  dsps: Int = 0,
  brams: Int = 0
)

case class PrefitResources (
  lut7: Int = 0,
  lut6: Int = 0,
  lut5: Int = 0,
  lut4: Int = 0,
  lut3: Int = 0,
  mem64: Int = 0,
  mem32: Int = 0,
  mem16: Int = 0,
  regs: Int = 0,
  dsps: Int = 0,
  brams: Int = 0
) {
  def +(that: PrefitResources) = PrefitResources(
    lut7  = this.lut7  + that.lut7,
    lut6  = this.lut6  + that.lut6,
    lut5  = this.lut5  + that.lut5,
    lut4  = this.lut4  + that.lut4,
    lut3  = this.lut3  + that.lut3,
    mem64 = this.mem64 + that.mem64,
    mem32 = this.mem32 + that.mem32,
    mem16 = this.mem16 + that.mem16,
    regs  = this.regs  + that.regs,
    dsps  = this.dsps  + that.dsps,
    brams = this.brams + that.brams
  )
  def *(a: Int) = PrefitResources(a*lut7,a*lut6,a*lut5,a*lut4,a*lut3,a*mem64,a*mem32,a*mem16,
                                  a*regs,a*dsps,a*brams)

  override def toString() = s"luts=$lut3,$lut4,$lut5,$lut6,$lut7,mems=$mem16,$mem32,$mem64,regs=$regs,dsps=$dsps,brams=$brams"
}

object NoArea extends PrefitResources()

object Model {
  val contentionModel = new ContentionModel()

  // TODO: At least some of this should really be read in from a config file
  def latencyOf(x: Node): Double = x match {
    case n: Const[_] => 0
    case n: Wire => 0
    case n: ArgIn => 0

    case n: ArgOut => 0
    case n: Reg => 1

    case n: Ctr => 0
    case n: BRAM => 0
    case n: OffChipArray => 0
    case n: DummyMemNode => 0
    case n: Bundle => 0
    case n: PQ => 0

    case n: Add => n.t match {
      case SInt(b) => 1
      case UInt(b) => 1
      case Flt() => 14
      case t => throw new Exception(s"TODO: Add cycle characterization for type $t")
    }
    case n: Sub => n.t match {
      case SInt(b) => 1
      case UInt(b) => 1
      case Flt() => 14
      case t => throw new Exception(s"TODO: Sub cycle characterization for type $t")
    }
    case n: Mul => n.t match {
      case SInt(32) => 2
      case UInt(32) => 2
      //case Flt() if n.op1 == n.op2 => println("Found square node $n"); 5
      case Flt() => 11
      case t => throw new Exception(s"TODO: Mul cycle characterization for type $t")
    }

    case n: Div => n.t match {
      case UInt(32) => 35
      case SInt(32) => 38
      case Flt() => 33
      case t => throw new Exception(s"TODO: Div cycle characterization for type $t")
    }
    case n: Mux => 1

    case n@Lt(x,y) => x.t match {
      case UInt(32) => 1
      case SInt(32) => 1
      case Flt() => 3
      case t => throw new Exception(s"TODO: Lt cycle characterization for type $t")
    }
    case n@Gt(x,y) => x.t match {
      case UInt(32) => 1
      case SInt(32) => 1
      case Flt() => 3
      case t => throw new Exception(s"TODO: Gt cycle characterization for type $t")
    }
    case n@Eq(x,y) => x.t match {
      case t: FixPt => 1
      case Flt() => 3
      case t => throw new Exception(s"TODO: Eq cycle characterization for type $t")
    }
    case n@Le(x,y) => x.t match {
      case UInt(32) => 1
      case SInt(32) => 1
      case Flt() => 3
      case t => throw new Exception(s"TODO: Le cycle characterization for type $t")
    }
    case n@Ge(x,y) => x.t match {
      case UInt(32) => 1
      case SInt(32) => 1
      case Flt() => 3
      case t => throw new Exception(s"TODO: Ge cycle characterization for type $t")
    }

    case n: MinWithMetadata => 1 // TODO: not correct for all types
    case n: MaxWithMetadata => 1 // TODO: not correct for all types

    case n: Float2Fix => (n.op.t, n.t) match {
      case (Flt(), UInt(32)) => 6
      case (Flt(), SInt(32)) => 6
      case (t1,t2) => throw new Exception(s"TODO: Float2Fix cycle characterization for type $t1 to type $t2")
    }
    case n: Fix2Float => (n.op.t, n.t) match {
      case (UInt(32), Flt()) => 6
      case (SInt(32), Flt()) => 6
      case (t1,t2) => throw new Exception(s"TODO: Fix2Float cycle characterization for type $t1 to type $t2")
    }

    case n: Abs => 1
    case n: Slice => 0

    case n: Log => n.t match {
      case Flt() => 35
      case t => throw new Exception(s"TODO: Log cycle characterization for type $t")
    }
    case n: Exp => n.t match {
      case Flt() => 27
      case t => throw new Exception(s"TODO: Exp cycle characterization for type $t")
    }
    case n: Sqrt => n.t match {
      case Flt() => 28
      case t => throw new Exception(s"TODO: Sqrt cycle characterization for type $t")
    }

    case n: Not => 1
    case n: And => 1
    case n: Or => 1

    case n@Ld(q: PQ, _, _) if q.dblBuf => 2     // f(r.banks)?
    case n@Ld(r: BRAM, _, _) if r.dblBuf => 2   // f(r.banks)?
    case n: Ld => 1               // f(r.banks)?
    case n: St => 1

    // TODO: Main memory cycle characterization...
    case n: TileMemLd =>
      //val burstSize = 384 // bytes
      //val burstCycles = 96
      //val bytes = (n.tileDim0 * n.tileDim1 * n.baseAddr.t.bits)/8
      //val cycles = (Math.ceil(bytes.toDouble/burstSize) * burstCycles).toInt
      val cycles = contentionModel.evaluate(n.contention, n.tileDim0, n.tileDim1)
      //println(s"$n: contention: ${n.contention}, rows: ${n.tileDim0}, cols; ${n.tileDim1}, cycles: $cycles")
      cycles

    // No contention effects for storers..
    case n: TileMemSt => n.tileDim0 * n.tileDim1 + 82

    case n: GraphNode => 0
    case n: ReduceTree => 0
    case n: CollectReduceTree => 0
    case n: Parallel => 1
    case n: Pipe => 1
    case n: MetaPipeline => 1
    case n: Sequential => 1

    case n => throw new Exception(s"No latency model for node $n")
  }

  def isPow2(x: Int): Boolean = (x & (x - 1)) == 0
  def isPow2(x: Long): Boolean = (x & (x - 1)) == 0

  // --- Area
  def areaOf(x: Node) = {
    val area = x match {
      // TODO: Not sure if this is accurate...
      case n: Const[_] =>
        val m64 = n.t.bits/64
        val m32 = (n.t.bits - m64*64)/32
        val m16 = (n.t.bits - m64*64 - m32*32)/16
        PrefitResources(mem64=m64,mem32=m32,mem16=m16)

      case n: Wire => PrefitResources()
      case n: Bundle => PrefitResources()
      case n: DummyMemNode => PrefitResources()

      case n: ArgIn => n.t.bits match {
        case 1 => PrefitResources(regs=5)
        case 32 => PrefitResources(regs=48)
        case 64 => PrefitResources(regs=96) // TODO
        case b => throw new Exception(s"TODO: ArgIn area characterization for $b bits")
      }

      case n: ArgOut => n.t.bits match {
        case 1 => PrefitResources(regs=5)
        case 32 => PrefitResources(regs=48)
        case 64 => PrefitResources(regs=96) // TODO
        case b => throw new Exception(s"TODO: ArgOut area characterization for $b bits")
      }

      case n: Reg if !n.dblBuf => PrefitResources(regs=n.t.bits)
      case n: Reg if n.dblBuf => PrefitResources(lut3=n.t.bits,regs=4*n.t.bits)

      case n: Ctr if n.isUnitCtr => PrefitResources(lut3=1,regs=1)
      case n: Ctr => PrefitResources(lut3=106*n.size,regs=67*n.size)

      case n: BRAM =>
        // Depth for given word size
        val wordDepth = if      (n.t.bits == 1)  16384
                        else if (n.t.bits == 2)  8192
                        else if (n.t.bits <= 5)  4096
                        else if (n.t.bits <= 10) 2048
                        else if (n.t.bits <= 20) 1024
                        else                     512

        val width = if (n.t.bits > 40) Math.ceil( n.t.bits / 40.0 ) else 1

        val rams = width.toInt * Math.ceil(n.depth.toDouble/(wordDepth*n.banks)).toInt * n.banks

        if (n.dblBuf) PrefitResources(lut3=2*n.t.bits, regs=2*n.t.bits, brams = rams*2)
        else          PrefitResources(brams = rams)

      case _: OffChipArray => PrefitResources()

      case Mul(Const(_),Const(_)) => PrefitResources()
      case Add(Const(_),Const(_)) => PrefitResources()
      case Sub(Const(_),Const(_)) => PrefitResources()
      case Div(Const(_),Const(_)) => PrefitResources()

      // Simple checks for multiplying and dividing by constant powers of 2
      case Mul(a, Const(x: Int)) if isPow2(x) == 0 => PrefitResources(regs = a.t.bits)
      case Mul(a, Const(x: Long)) if isPow2(x) == 0 => PrefitResources(regs = a.t.bits)
      case Mul(Const(x: Int), a) if isPow2(x) == 0 => PrefitResources(regs = a.t.bits)
      case Mul(Const(x: Long), a) if isPow2(x) == 0  => PrefitResources(regs = a.t.bits)
      case Div(a, Const(x: Int)) if isPow2(x) == 0 => PrefitResources(regs = a.t.bits)
      case Div(a, Const(x: Long)) if isPow2(x) == 0 => PrefitResources(regs = a.t.bits)

      case Mul(a, Const(x: Int)) => PrefitResources(regs = a.t.bits)
      case Mul(a, Const(x: Long)) => PrefitResources(regs = a.t.bits)
      case Mul(Const(x: Int), a) => PrefitResources(regs = a.t.bits)
      case Mul(Const(x: Long), a) => PrefitResources(regs = a.t.bits)

      // TODO: Overestimates area when one of the arguments is a constant value
      case n: Add => n.t match {
        case SInt(b) => PrefitResources(lut3=b,regs=b)
        case UInt(b) => PrefitResources(lut3=b,regs=b)
        case Flt() => PrefitResources(lut3=397,lut4=29,lut5=125,lut6=34,lut7=5,regs=606,mem16=50)
        case t => throw new Exception(s"TODO: Add area characterization for type $t")
      }
      case n: Sub => n.t match {
        case SInt(b) => PrefitResources(lut3=b,regs=b)
        case UInt(b) => PrefitResources(lut3=b,regs=b)
        case Flt() => PrefitResources(lut3=397,lut4=29,lut5=125,lut6=34,lut7=5,regs=606,mem16=50)
        case t => throw new Exception(s"TODO: Sub area characterization for type $t")
      }


      case n: Mul => n.t match {
        case SInt(32) => PrefitResources(dsps=2)
        case UInt(32) => PrefitResources(dsps=2)
        case Flt() => PrefitResources(lut3=152,lut4=10,lut5=21,lut6=2,dsps=1,regs=335,mem16=43)
        case t => throw new Exception(s"TODO: Mul area characterization for type $t")
      }
      case n: Div => n.t match {
        case UInt(32) => PrefitResources(lut3=1192,lut5=2,regs=2700)
        case SInt(32) => PrefitResources(lut3=1317,lut5=6,regs=2900)
        case Flt() => PrefitResources(lut3=2384,lut4=448,lut5=149,lut6=385,lut7=1,regs=3048,mem32=25,mem16=9)
        case t => throw new Exception(s"TODO: Div area characterization for type $t")
      }
      case n: Mux => PrefitResources(regs=n.t.bits)

      case n@Lt(x,y) => x.t match {
        case UInt(32) => PrefitResources(lut4=30,regs=1)
        case SInt(32) => PrefitResources(lut4=30,regs=1)
        case Flt() => PrefitResources(lut4=42,lut6=26,regs=33)
        case t => throw new Exception(s"TODO: Lt area characterization for type $t")
      }
      case n@Gt(x,y) => x.t match {
        case UInt(32) => PrefitResources(lut4=30,regs=1)
        case SInt(32) => PrefitResources(lut4=30,regs=1)
        case Flt() => PrefitResources(lut4=42,lut6=26,regs=33)
        case t => throw new Exception(s"TODO: Gt area characterization for type $t")
      }
      case n@Eq(x,y) => x.t match {
        case UInt(1) => PrefitResources(lut4=1,regs=1)
        case UInt(32) => PrefitResources(lut4=13,lut5=4,regs=1) // Roughly correct?
        case SInt(32) => PrefitResources(lut4=13,lut5=4,regs=1)
        case Flt() => PrefitResources(lut4=42,lut6=26,regs=33)
        case t => throw new Exception(s"TODO: Eq area characterization for type $t")
      }
      case n@Le(x,y) => x.t match {
        case UInt(32) => PrefitResources(lut4=30,regs=1)
        case SInt(32) => PrefitResources(lut4=30,regs=1)
        case Flt() => PrefitResources(lut4=42,lut6=26,regs=33)
        case t => throw new Exception(s"TODO: Le area characterization for type $t")
      }
      case n@Ge(x,y) => x.t match {
        case UInt(32) => PrefitResources(lut4=30,regs=1)
        case SInt(32) => PrefitResources(lut4=30,regs=1)
        case Flt() => PrefitResources(lut4=42,lut6=26,regs=33)
        case t => throw new Exception(s"TODO: Ge area characterization for type $t")
      }

      case n: MinWithMetadata => PrefitResources(lut3=96,regs=96) // TODO: not correct for all types
      case n: MaxWithMetadata => PrefitResources(lut3=96,regs=96) // TODO: not correct for all types

      case n: Float2Fix => (n.op.t, n.t) match {
        case (Flt(), UInt(32)) => PrefitResources(lut4=160,lut6=96,regs=255)
        case (t1,t2) => throw new Exception(s"TODO: Float2Fix area characterization for type $t1 to type $t2")
      }
      case n: Fix2Float => (n.op.t, n.t) match {
        case (UInt(32), Flt()) => PrefitResources(lut4=50,lut6=132,regs=238)
        case (t1,t2) => throw new Exception(s"TODO: Fix2Float area characterization for type $t1 to type $t2")
      }

      case n@Not(x) => PrefitResources(lut3=x.t.bits,regs=x.t.bits)
      case n@And(x,y) => PrefitResources(lut3=x.t.bits,regs=x.t.bits)
      case n@Or(x,y) => PrefitResources(lut3=x.t.bits,regs=x.t.bits)

      case n: Log => n.t match {
        // This is specifically for the case where we don't know the min/max values of the input
        case Flt() => PrefitResources(lut3=472,lut4=47,lut5=74,lut6=26,lut7=3,mem16=42,regs=950,dsps=7,brams=3)
        case t => throw new Exception(s"TODO: Log area characterization for type $t")
      }
      case n: Abs => n.t match {
        case t: FloatPt => PrefitResources(regs = t.bits - 1)
        case t => throw new Exception(s"TODO: Abs area characterization for type $t")
      }
      case n: Exp => n.t match {
        case Flt() => PrefitResources(lut3=368,lut4=102,lut5=137,lut6=38,mem16=24,regs=670,dsps=5,brams=2)
        case t => throw new Exception(s"TODO: Exp area characterization for type $t")
      }
      case n: Sqrt => n.t match {
        case Flt() => PrefitResources(lut3=476,lut4=6,lut5=6,mem32=11,regs=900)
        case t => throw new Exception(s"TODO: Sqrt area characterization for type $t")
      }

      case n: Slice => PrefitResources()

      case n: PQ => PrefitResources(lut3=8,lut4=11,lut5=10,lut6=9,regs=129)*n.depth

      // TODO: These are close but could still use some refining
      case n@Ld(mem, _, _) if isPow2(mem.banks) =>
        val luts = mem.t.bits
        val regs = mem.t.bits
        PrefitResources(lut3=luts,regs=regs)
      case n@St(mem, _, _, _, _) if isPow2(mem.banks) =>
        val luts = mem.t.bits
        val regs = mem.t.bits
        PrefitResources(lut3=luts,regs=regs)

      case n@Ld(mem, _, _) =>
        val luts = mem.t.bits + Math.ceil(Math.log(mem.banks)).toInt
        val regs = mem.t.bits + Math.ceil(Math.log(mem.banks)).toInt
        PrefitResources(lut3=luts,regs=regs)
      case n@St(mem, _, _, _, _) =>
        val luts = mem.t.bits + Math.ceil(Math.log(mem.banks)).toInt
        val regs = mem.t.bits + Math.ceil(Math.log(mem.banks)).toInt
        PrefitResources(lut3=luts,regs=regs)


      // TODO: Change these over for 2014.1
      case n: TileMemLd =>
        val dsp = if (List(n.colDim, n.idx0,n.idx1).filterNot(_.isInstanceOf[Const[_]]).size > 1) 4 else 0
        PrefitResources(lut3=453, lut4=60, lut5=131,lut6=522,regs=1377,dsps=dsp,brams=46) // 2014.1 was dsp=4
        //PrefitResources(lut3=2698,lut4=161,lut5=169,lut6=464,regs=4450,dsps=4,brams=46,mem32=30,mem16=2) // 2014.2
      case n: TileMemSt =>
        val dsp = if (List(n.colDim, n.idx0,n.idx1).filterNot(_.isInstanceOf[Const[_]]).size > 1) 3 else 0
        PrefitResources(lut3=1900,lut4=167,lut5=207,lut6=516,lut7=11,regs=5636,dsps=dsp,brams=46) // 2014.1 was dsp=3
        //PrefitResources(lut3=3220,lut4=214,lut5=80,lut6=677,regs=7750,dsps=3,brams=46) // 2014.2

      case n: GraphNode => PrefitResources()
      case n: ReduceTree => PrefitResources()
      case n: CollectReduceTree => PrefitResources()
      case n: Parallel => PrefitResources(lut4=9*n.size/2, regs=n.size + 3)
      case n: Pipe => PrefitResources()
      case n: MetaPipeline =>
        PrefitResources(
          lut4 = (11*n.size*n.size + 45*n.size)/2 + 105,    // 0.5( 11n^2 + 45n ) + 105
          regs = (n.size*n.size + 3*n.size)/2 + 35          // 0.5( n^2 + 3n ) + 35
        )
      case n: Sequential => PrefitResources(lut4=7*n.size+40, regs=2*n.size+35)

      case n => throw new Exception(s"No area model for node $n")
    }
    x match {
      case x: CombNode if x.vecWidth > 1 => area * x.vecWidth // Multiply combinational nodes by vector size for maps in pipelines
      case _ => area
    }
  }
}