package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.{Design, Config}
import java.io.PrintWriter

class CycleAnalysis(implicit design: Design) extends Traversal {

  val interruptCycles = 96
  val flushCycles = 512
  val pcieCycles = 42500
  val baseCycles = flushCycles + interruptCycles + pcieCycles
  override val debug = true

  def latencyOf(node: Node): Double = Model.latencyOf(node)

  override def initPass() {
    super.initPass()
    cycleScope ::= baseCycles
  }

  var cycleScope: List[Double] = Nil

  def inScope(x: => Unit) = {
    val outerScope = cycleScope
    cycleScope = Nil
    x
    val retList = cycleScope
    cycleScope = outerScope
    (retList)
  }

  def inSeqScope(x: => Unit) = {
    val outerScope = cycleScope
    cycleScope = Nil
    x
    val cycles = cycleScope.reduce{(a,b) => a + b}
    cycleScope = outerScope
    (cycles)
  }

  def bound(x: CombNode): Double = design.nodeBounds.getOrElse(x, 1)

  def cycleLatency(x: Node) = x match {
    case n: Add if n.t == Flt() => 2
    case _ => latencyOf(x)
  }

  // TODO: When are maxes for counters evaluated? Every outer loop iteration?
  // TODO: Approximate the (relative?) number of iterations for loops?
  override def visitNode(node: Node) { if (!visited.contains(node)) {
    visited += node
    val cycles = node match {

      case n: ReduceTree =>
        val allNodes = n.graph.flatMap(_.nodes)

        def dfs_cycle(nodes: List[Node]): List[Node] = {
          val deps = nodes.head.deps
          if (deps.contains(n.accum)) nodes
          else {
            var out: List[Node] = Nil
            var i = 0
            while (out.isEmpty && i < deps.length) {
              val dep = deps(i)
              if (allNodes.contains(dep)) out = dfs_cycle(dep +: nodes)
            }
            out
          }
        }
        val cycle = dfs_cycle(List(n.accum))

        def dfs_tree(node: Node): Double = {
          if (allNodes.contains(node) && !cycle.contains(node))
            latencyOf(node) + node.deps.map(dfs_tree).fold(0.0){(a,b) => if (a > b) a else b}
          else 0
        }

        val cycleCycles = if (cycle.length == 2) 1 else cycle.map(cycleLatency).sum
        val extraReduce = if (cycle.length == 2) cycle.map(cycleLatency).sum else 0
        cycleScope ::= cycleCycles

        val mapCycles = if (Config.quick) extraReduce
                        else cycle.map(x => x.deps.map(dfs_tree).max).max + extraReduce

        dbg(s"$n reduction tree: cycle = $cycleCycles, map = $mapCycles")
        mapCycles

      // Identify length of critical path in combinational logic
      case n: GraphNode if n.isEmpty => 0

      case n: GraphNode =>
        val sched = n.topSort
        def dfs_latency(node: Node): Double = {
          if (sched.contains(node))
            latencyOf(node) + node.deps.map(dfs_latency).fold(0.0){(a,b) => if (a > b) a else b}
          else 0
        }

        val latencies = n.getouts.map(dfs_latency)
        //msg("Latencies: " + latencies.mkString(", "))
        latencies.max

      case n: Parallel =>
        val body = inScope { n.nodes.foreach(visitNode) }
        body.max + latencyOf(n)

      case n: Pipe =>
        val N = Math.floor(bound(n.iters))
        dbg("------------------")
        dbg(s"$n: $N iterations")

        val initTime = 1 //inScope { n.ctr.max.foreach(visitNode) }.max
        val map = inSeqScope { visitNode(n.mapNode) }

        if (n.hasReduce) {
          val red = inScope { visitNode(n.reduceNode.get) }
          val reduceCycle = red(1)
          var nLevels = 0
          val reduceTree = if (Config.quick) {
            nLevels = Math.ceil(Math.log(n.ctr.par.reduce{_*_})/Math.log(2.0)).toInt
            red(0) * nLevels
          } else red(0)

          dbg(s"Reduction pipe $n tree cycles: $reduceTree, accum. cycles: $reduceCycle, par: ${n.ctr.par.reduce{_*_}}, levels = $nLevels")
          dbg(s"} end of $n")
          initTime + map + reduceTree + N*reduceCycle
        }
        else {
          dbg(s"} end of $n")
          initTime + map + N - 1
        }

      case n: MetaPipeline =>
        dbg("------------------")
        val N = Math.floor(bound(n.iters))
        dbg(s"$n: $N iterations")

        val initTime = 1 //inScope { n.ctr.max.foreach(visitNode) }.max
        val ctr = 0 //inSeqScope { visitNode(n.ctr) }
        val body = inScope { n.nodes.foreach(visitNode) }
        dbg(s"} end of $n (body.max = ${body.max}, body.sum = ${body.sum}")
        initTime + (N-1)*body.max + body.sum //+ N*latencyOf(n)

      case n: Sequential =>
        dbg("------------------")
        val N = Math.floor(bound(n.iters))
        dbg(s"$n: $N iterations {")

        val initTime = 1 //inScope { n.ctr.max.foreach(visitNode) }
        val ctr = inSeqScope { visitNode(n.ctr) }
        val body = inSeqScope { n.nodes.foreach(visitNode) }

        dbg(s"} end of $n")
        initTime + N*ctr + N*body //+ N*latencyOf(n)

      case _ => latencyOf(node)
    }
    dbg(s"$node: $cycles")
    cycleScope ::= cycles
  }}

  def analyze(top: Node) = {
    cycleScope = Nil
    run(top)
    cycleScope.reduce{_+_}
  }
}
