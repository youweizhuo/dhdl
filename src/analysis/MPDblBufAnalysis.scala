package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design

class MPDblBufAnalysis(implicit design: Design) extends Traversal {

  override def visitNode(node: Node) = {
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n: TileMemSt =>
          if (n.buf.isDblBuf) {
  //          println(s"""Setting writer of ${n.buf} to ${n}""")
            n.buf.addReader(n)
          }
        case n: TileMemLd =>
          n.buf.map { b =>
            if (b.isDblBuf) {
  //            println(s"""Adding reader $n to ${n.buf}""")
              b.setWriter(n)
            }
          }
        case n@St(mem, addr, data, start, stride) =>
          if (mem.isDblBuf) {
            assert(n.getParent() != null, s"""
              Parent of $n is null, cannot set reader to dblbuf $mem.
              Did you forget to add $n to GraphNode()?""")
            mem.setWriter(n.getParent())
          }
        case n@Ld(mem, addr, stride) =>
          if (mem.isDblBuf) {
            assert(n.getParent() != null, s"""
              Parent of $n is null, cannot set reader to dblbuf $mem.
              Did you forget to add $n to GraphNode()?""")
            mem.addReader(n.getParent())
          }
        case _ =>
          super.visitNode(node)
      }
    }
  }
}
