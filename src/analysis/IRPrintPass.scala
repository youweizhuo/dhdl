package dhdl.analysis

import dhdl.graph.traversal.Traversal
import dhdl.graph._
import dhdl.Design
import scala.collection.mutable.Set

class IRPrintPass(implicit design: Design) extends Traversal {

  def p(x: String) = println(x)

  override def visitNode(node: Node) = {
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n@Not(op) =>
          visitNode(op)
          p(s"$n = Not($op)")
        case n@Add(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          p(s"$n = Add($op1, $op2)")
        case n@Mul(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          p(s"$n = Mul($op1, $op2)")
        case n@Div(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          p(s"$n = Div($op1, $op2)")
        case n@Lt(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          p(s"$n = Lt($op1, $op2)")
        case n@Mux(sel, i1, i2) =>
          visitNode(sel)
          visitNode(i1)
          visitNode(i2)
          p(s"$n = Mux($sel, $i1, $i2)")
        case n@ArgIn(id) =>
          p(s"$n = ArgIn($id)")
        case n@ArgOut(id,value) =>
          visitNode(value)
          p(s"$n = ArgOut($id, $value)")
        case n@Ld(mem, addr, stride) =>
          visitNode(mem)
          visitNode(addr)
          p(s"$n = Ld($mem, $addr, $stride)")
        case n@St(mem, addr, data, start, stride) =>
          visitNode(mem)
          visitNode(addr)
          visitNode(data)
          p(s"$n = St($mem, $addr, $data)")
        case n@Const(value) =>
          p(s"$n = Const($value)")
        case n@Slice(in, from, to) =>
          visitNode(in)
          p(s"$n = Slice($in, $from, $to)")
        case n: Ctr =>
          for (max <- n.max) {
            visitNode(max)
          }
          p(s"$n = Ctr(${n.max})")
        case n@GraphNode(nodes @ _*) =>
          n.deps.map(visitNode(_))
          p(s"GraphNode $n {")
          val sched = n.topSort
          sched.map(visitNode(_))
          p(s"} GraphNode $n")
        case n@ReduceTree(oldval, accum, graph @_*) =>
          visitNode(oldval)
          visitNode(accum)
          p(s"ReduceTree $n {")
          graph.map { visitNode(_) }
          p(s"} ReduceTree $n")
        case n@CollectReduceTree(in, accum, body @_*) =>
          in.map { visitNode(_) }
          visitNode(accum)
          p(s"CollectReduceTree $n {")
          body.map { visitNode(_) }
          p(s"} CollectReduceTree $n")
        case n@Pipe(ctr, mapNode, redNode) =>
          p(s"Pipe $n {")
          visitNode(ctr)
          visitNode(mapNode)
          if (n.hasReduce) visitNode(redNode.get)
          p(s"} Pipe $n")
        case n@Parallel(nodes @ _*) =>
          p(s"Parallel $n {")
          for (node <- n.nodes) {
            visitNode(node)
          }
          p(s"} Parallel $n")
        case n: TileMemLd =>
          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          n.buf.map {visitNode(_) }
          visitNode(n.force)
          p(s"$n = TileMemLd")
        case n: TileMemSt =>
          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          visitNode(n.buf)
          visitNode(n.force)
          p(s"$n = TileMemSt")
        case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
          p(s"MetaPipeline $n {")
          visitNode(numIter)
          visitNode(ctr)
          visitNode(regChain)
          for (node <- n.nodes) {
            visitNode(node)
          }
          p(s"} MetaPipeline $n")
        case n@Sequential(ctr, numIter, nodes @ _*) =>
          p(s"Sequential $n {")
          visitNode(numIter)
          visitNode(ctr)
          for (node <- n.nodes) {
            visitNode(node)
          }
          p(s"} Sequential $n")
        case n@BRAM(depth, isDblBuf) =>
          p(s"$n = BRAM($depth, $isDblBuf)")
        case n@Reg(isDblBuf, init) =>
          n.deps.map {visitNode(_)}
          if (init.isDefined) {
            p(s"$n = Reg($isDblBuf, ${init.get})")
          } else {
            p(s"$n = Reg($isDblBuf)")
          }
        case n@DummyCtrlNode() =>
          p(s"$n = DummyCtrlNode()")
        case n: DummyMemNode =>
          p(s"$n = DummyMemNode()")
        case n: Wire =>
          p(s"$n = Wire()")
        case n@OffChipArray(_, sizes @ _*) =>
          sizes.map(visitNode(_))
          p(s"$n = OffChipArray($sizes)")
        case n@PE(deps @ _*) =>
          deps.map(visitNode(_))
        case _ =>
          throw new Exception(s"Don't know how to visit $node")
      }
    }
  }
}
