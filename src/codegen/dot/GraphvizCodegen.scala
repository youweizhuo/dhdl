package dhdl.codegen.dot

import dhdl.codegen.Codegen
import dhdl.graph._
import java.io.PrintWriter
import dhdl.Design

class GraphvizCodegen(file: String = "outfile")(implicit design: Design) extends Codegen {
  override val ext = "dot"

  // Constants
  val arrowSize = 0.6
  val edgeThickness = 0.5
  val memColor = "#6ce6e1"
  val regColor = "#8bd645"
  val offChipColor = "#1A0000"
  val dblbufBorderColor = "#4fb0b0"
  val ctrlColor = "red"
  val counterColor = "#e8e8e8"
  val counterInnerColor = "gray"
  val fontsize = 10
  val defaultShape = "square"
  val bgcolor = "white"

  // Metapipeline colors
  val mpFillColor = "#4FA1DB"
  val mpBorderColor = "#4FA1DB"
  val mpStageFillColor = "#BADDFF"
  val mpStageBorderColor = "none"

  // Parallel colors
  val parallelFillColor = "#4FDBC2"
  val parallelBorderColor = "#00AB8C"
  val parallelStageFillColor = "#CCFFF6"
  val parallelStageBorderColor = "none"

  val outfileName = s"$file.$ext"
  override def clearOldFiles = false

  // Initialized in initPass
  // TODO: Is there any way I can avoid using var?
  var pw: PrintWriter = null

  def fp(a: Any) = pw.println(a)


  override def run(node: Node) = {
    // Always call initPass first
    initPass()

    // First print all arg nodes and mem nodes
    val argInNodes = design.allNodes.filter(_.isInstanceOf[ArgIn])
    val constnodes = design.allNodes.filter(_.isInstanceOf[Const[_]])
    val offChipNodes = design.allNodes.filter(_.isInstanceOf[OffChipArray])
    argInNodes.map(visitNode(_))
    constnodes.map(visitNode(_))
    offChipNodes.map(visitNode(_))

    visitNode(node)

    val argOutNodes = design.allNodes.filter(_.isInstanceOf[ArgOut])
    argOutNodes.map(visitNode(_))

    finPass()
  }

  override def initPass() = {
    super.initPass()
    copyFile(s"$filesDir/Makefile", s"$outdir/Makefile")
    pw = new PrintWriter(s"$outdir/$outfileName")
    fp(s"digraph {")
    fp(s"compound=true")
    fp(s"""graph [splines=\"ortho\" clusterrank=\"local\"]""")
    fp(s"edge [arrowsize=$arrowSize penwidth=$edgeThickness]")
    fp(s"""node [fontsize=$fontsize shape=$defaultShape style=\"filled\" fillcolor=\"$bgcolor\"]""")
    fp(s"fontsize=$fontsize")
  }

  override def finPass() = {
    fp(s"}")
    pw.flush()
    pw.close()
  }

  override def visitNode(node: Node) : Unit = {
    if (!isInit) {
      initPass()
    }
    if (!visited.contains(node)) {
      visited += node
      node match {
        case n@Fix2Float(op, t) =>
          visitNode(op)
          fp(s"""$n [label=\"$n\"]""")
          fp(s"""$op -> $n""")
        case n@Float2Fix(op, t) =>
          visitNode(op)
          fp(s"""$n [label=\"$n\"]""")
          fp(s"""$op -> $n""")
        case n@Not(op) =>
          visitNode(op)
          fp(s"""$n [label=\"~\"]""")
          fp(s"""$op -> $n""")
        case n@Add(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"+\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Sub(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"-\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Mul(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"*\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Div(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"/\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Lt(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"<\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Gt(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\">\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Eq(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"==\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Le(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"<=\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Ge(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\">=\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        // TODO: This only works on Long, Int, and Boolean
        case n@And(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"&\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Or(op1,op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"|\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Abs(op) =>
          visitNode(op)
          fp(s"""$n [label=\"abs\"]""")
          fp(s"""$op -> $n""")

        case n@Exp(op) =>
          visitNode(op)
          fp(s"""$n [label=\"exp\"]""")
          fp(s"""$op -> $n""")

         case n@Log(op) =>
          visitNode(op)
          fp(s"""$n [label=\"log\"]""")
          fp(s"""$op -> $n""")

        case n@Sqrt(op) =>
          visitNode(op)
          fp(s"""$n [label=\"sqrt\"]""")
          fp(s"""$op -> $n""")

       case n@Mux(sel, i1, i2) =>
          visitNode(sel)
          visitNode(i1)
          visitNode(i2)
          fp(s""" $n [label=\"$n\" shape=\"diamond\"]""")
          fp(s""" $sel -> $n""")
          fp(s""" $i1  -> $n""")
          fp(s""" $i2  -> $n""")

        case n@Bundle(elems @_*) =>
          elems.map { visitNode(_) }
          fp(s""" $n [label=\"$n\"] """)
          elems.map { e => fp(s"""$e -> $n""") }

        case n@MinWithMetadata(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"minWithMD\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@MaxWithMetadata(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"maxWithMD\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n@Max(op1, op2) =>
          visitNode(op1)
          visitNode(op2)
          fp(s"""$n [label=\"max\"]""")
          fp(s"""$op1 -> $n""")
          fp(s"""$op2 -> $n""")

        case n: ArgIn =>
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label = \"\"""")
          fp(s"""color = \"white\"""")
          fp(s"""$n[label=\"$n\" shape=\"Msquare\"]""")
          fp("}")

        case n@ArgOut(id,value) =>
          visitNode(value)
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label = \"\"""")
          fp(s"""color = \"white\"""")
          fp(s"""$n[label=\"$n\" shape=\"Msquare\"]""")
          fp("}")
          fp(s"""$value -> $n""")

        case n@Ld(mem, addr, stride) =>
          visitNode(mem)
          visitNode(addr)
          fp(s"""$n [label=\"ld\"]""")
          if (mem.isDblBuf) {
            fp(s"""$mem:ld -> $n""")
          } else {
            fp(s"""$mem -> $n""")
          }
          fp(s"""$addr -> $n""")

          n.compositeValues.map { v =>
            visitNode(v)
            fp(s"""$n -> $v""")
          }
        case n@St(mem, addr, data, start, stride) =>
          visitNode(mem)
          visitNode(addr)
          visitNode(data)
          fp(s"""$n [label=\"st\"]""")
          fp(s"""$addr -> $n""")
          fp(s"""$data -> $n""")
          if (mem.isDblBuf) {
            fp(s"""$n -> $mem:st""")
          } else {
            fp(s"""$n -> $mem""")
          }
        case n@Const(value) =>
          fp(s"""$n [label=\"$value\" style=\"filled\" fillcolor=\"lightgray\" color=\"none\"]""")
        case n@Slice(in, from, to) =>
          visitNode(in)
          fp(s"""$n [label=\"slice_$from\" shape="invtriangle" margin=0 width=0 height=0]""")
          fp(s"""$in -> $n""")
        case n:Ctr =>
          for (max <- n.max) {
            visitNode(max)
          }
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n\"""")
          fp(s"""style=\"rounded, filled\"""")
          fp(s"""fillcolor=\"$counterColor\"""")
          for (i <- 0 until n.ctrs.size) {
            val ctr = n.ctrs(i)
            fp(s"""$ctr [label=\"ctr$i\" shape=\"box\" style=\"filled,rounded\" color=\"$counterInnerColor\" height=0 width=0]""")
          }
          fp("}")
          for (i <- 0 until n.ctrs.size) {
            val max = n.max(i)
            val ctr = n.ctrs(i)
            fp(s"$max -> $ctr [lhead=cluster_$n]")
          }
        case n@GraphNode(nodes @ _*) =>
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n\"""")
          fp(s"""style=\"filled\"""")
          fp(s"""fillcolor=\"green\"""")
          fp(s"""cluster_$n [label="" style="invisible" height=0 width=0 margin=0]""")
          val sched = n.topSort
          sched.map(visitNode(_))
          fp("}")
          n.deps.map(visitNode(_))
        case n@ReduceTree(oldval, accum, graph @ _*) =>
          visitNode(oldval)
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n\"""")
          fp(s"""style=\"filled\"""")
          fp(s"""fillcolor=\"darkgreen\"""")
          fp(s"""cluster_$n [label="" style="invisible" height=0 width=0 margin=0]""")
          graph.map(visitNode(_))
          fp("}")
          visitNode(accum)
        case n@CollectReduceTree(in, accum, body @ _*) =>
          in.map { visitNode(_) }
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n\"""")
          fp(s"""style=\"filled\"""")
          fp(s"""fillcolor=\"darkred\"""")
          fp(s"""cluster_$n [label="" style="invisible" height=0 width=0 margin=0]""")
          body.map(visitNode(_))
          fp("}")
          visitNode(accum)


        case n@Pipe(ctr, mapNode, redNode) =>
          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n (${n.par}) ${n.getParent()}\"""")
          fp(s"""color=\"gray\"""")
          visitNode(ctr)
          visitNode(mapNode)
          if (n.hasReduce) visitNode(redNode.get)
          fp("}")
        case n@Parallel(nodes @ _*) =>
          fp(s"subgraph cluster_$n {")
          fp(s"""label = \"$n ${n.getParent()}\"""")
          fp(s"""style = \"filled\"""")
          fp(s"""fillcolor = \"$parallelFillColor\"""")
          fp(s"""color = \"$parallelBorderColor\"""")
          var s = 0
          for (node <- n.nodes) {
            fp(s"subgraph cluster_${n}_s$s {")
            fp(s"""label=\"${n}_s$s\"""")
            fp(s"""style = \"filled\"""")
            fp(s"""fillcolor = \"$parallelStageFillColor\"""")
            fp(s"""color = \"$parallelStageBorderColor\"""")
            fp(s"""cluster_${n}_s$s [label="0" shape=\"box\" width=0 height=0 margin=0 style=\"invisible\"]""")
            visitNode(node)
            fp(s"}")
            s += 1
          }
          fp("}")
        case n: TileMemLd =>
          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          n.buf.map { visitNode(_) }
          visitNode(n.force)

          fp(s"""subgraph cluster_$n {""")
          fp(s"""label=\"$n\"""")
          fp(s"""style=\"rounded, filled\"""")
          fp(s"""fillcolor=\"white\"""")
          fp(s"""color=\"black\"""")
          fp(s"""$n [label=\"\" style=\"invisible\"]""")
          fp(s"""${n.isLoading} [label=\"isLoading\" style=\"rounded, filled\" fillcolor=\"gray\" height=0 width=0]""")
          fp(s"}")
          fp(s"""${n.baseAddr} -> $n [lhead=cluster_$n]""")
          fp(s"""${n.colDim} -> $n [lhead=cluster_$n]""")
          fp(s"""${n.idx0} -> $n [lhead=cluster_$n]""")
          fp(s"""${n.idx1} -> $n [lhead=cluster_$n]""")
          fp(s"""${n.force} -> $n [lhead=cluster_$n]""")
          n.buf.map { b =>
            if (b.isDblBuf) {
              fp(s"""$n -> ${b}:st [ltail=cluster_$n]""")
            } else {
              fp(s"""$n -> ${b} [ltail=cluster_$n]""")
            }
          }
        case n: TileMemSt =>
          visitNode(n.baseAddr)
          visitNode(n.colDim)
          visitNode(n.idx0)
          visitNode(n.idx1)
          visitNode(n.buf)
          visitNode(n.force)
          fp(s"""$n [label=\"TileMemSt\" shape=\"box\" style=\"rounded, filled\" margin=0.4]""")
          fp(s"""$n -> ${n.baseAddr}""")
          fp(s"""${n.colDim} -> $n""")
          fp(s"""${n.idx0} -> $n""")
          fp(s"""${n.idx1} -> $n""")
          fp(s"""${n.force} -> $n""")
          if (n.buf.isDblBuf) {
            fp(s"""${n.buf}:ld -> $n""")
          } else {
            fp(s"""${n.buf} -> $n""")
          }

        case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
          fp(s"subgraph cluster_$n {")
          fp(s"""label = \"$n ${n.getParent()}\"""")
          fp(s"""style = \"filled\"""")
          fp(s"""fillcolor = \"$mpFillColor\"""")
          fp(s"""color = \"$mpBorderColor\"""")
          fp(s"""${n}_ctrl [label="ctrl" height=0 style="filled" fillcolor="$mpBorderColor"]""")
          var s = 0
          visitNode(numIter)
          fp(s"""cluster_${n.numIter} -> ${n}_ctrl [ltail=cluster_${n.numIter}]""")
          visitNode(ctr)
          visitNode(regChain)
          for (node <- n.nodes) {
            fp(s"subgraph cluster_${n}_s$s {")
            fp(s"""label=\"${n}_s$s\"""")
            fp(s"""style = \"filled\"""")
            fp(s"""fillcolor = \"$mpStageFillColor\"""")
            fp(s"""color = \"$mpStageBorderColor\"""")
            fp(s"""cluster_${n}_s$s [label="0" shape=\"box\" width=0 height=0 margin=0 style=\"invisible\"]""")
            visitNode(node)
            fp(s"}")
            s += 1
          }
          val ctrlEdges = (0 until n.nodes.length-1 toList)
                          .map(i => (s"cluster_${n}_s$i", s"cluster_${n}_s${i+1}"))
                          .map(t => s"""${t._1} -> ${t._2} [color=\"$mpStageFillColor\" ltail=${t._1} lhead=${t._2} penwidth=1.5 arrowsize=1]""")
          ctrlEdges.foreach{fp(_)}
          fp("}")
        case n@Sequential(ctr, numIter, nodes @ _*) =>
          fp(s"subgraph cluster_$n {")
          fp(s"""label = \"$n\"""")
          fp(s"""${n}_ctrl [label="ctrl" height=0 style="filled" color="$ctrlColor" fillcolor="$ctrlColor"]""")
          var s = 0
          visitNode(numIter)
          fp(s"""${n.iters} -> ${n}_ctrl""")
          visitNode(ctr)
          for (node <- n.nodes) {
            fp(s"subgraph cluster_${n}_s$s {")
            fp(s"""label=\"${n}_s$s\"""")
            fp(s"""color=\"$ctrlColor\"""")
            fp(s"""cluster_${n}_s$s [label="0" shape=\"box\" width=0 height=0 margin=0 style=\"invisible\"]""")
            visitNode(node)
            fp(s"}")
            fp(s"""${n}_ctrl -> cluster_${n}_s$s [color=\"$ctrlColor\" lhead=cluster_${n}_s$s]""")
            s += 1
          }
//          val ctrlEdges = (0 until n.nodes.length-1 toList)
//                          .map(i => (s"cluster_${n}_s$i", s"cluster_${n}_s${i+1}"))
//                          .map(t => s"""${t._1} -> ${t._2} [color=\"$ctrlColor\" ltail=${t._1} lhead=${t._2}]""")
//          ctrlEdges.foreach{fp(_)}
          fp("}")
        case n@BRAM(depth, isDblBuf) =>
          fp(s"""subgraph cluster_$n {""")
          fp(s"""color=\"none\"""")
          fp(s"""label=\"\"""")
          if (isDblBuf) {
            fp(s"""$n [margin=0 rankdir=\"LR\" label=\"{<st> $n | <ld> }\" shape=\"record\" color=\"$dblbufBorderColor\" style=\"filled\" fillcolor=\"$memColor\"]""")
          } else {
            fp(s"""$n [label=\"$n\" shape=\"square\" style=\"filled\" fillcolor=\"$memColor\"]""")
          }
          fp(s"""}""")
        case n@PQ(depth, isDblBuf) =>
          fp(s"""subgraph cluster_$n {""")
          fp(s"""color=\"none\"""")
          fp(s"""label=\"\"""")
          if (isDblBuf) {
            fp(s"""$n [margin=0 rankdir=\"LR\" label=\"{<st> $n | <ld> }\" shape=\"record\" color=\"orange\" style=\"filled\" fillcolor=\"yellow\"]""")
          } else {
            fp(s"""$n [label=\"$n\" shape=\"square\" style=\"filled\" fillcolor=\"yellow\"]""")
          }
          fp(s"""}""")

        case n@Reg(isDblBuf,init) =>
          n.deps.map(visitNode(_))
          fp(s"""subgraph cluster_$n {""")
          fp(s"""color=\"none\"""")
          fp(s"""label=\"\"""")
          if (isDblBuf) {
            fp(s"""$n [margin=0 rankdir=\"LR\" label=\"{<st> | <ld> }\" shape=\"record\" color=\"$dblbufBorderColor\" style=\"filled\" fillcolor=\"$regColor\"]""")
          } else {
            fp(s"""$n [label=\"$n\" shape=\"square\" color=\"$regColor\" style=\"filled\" fillcolor=\"$regColor\"]""")
          }
          fp(s"""}""")
          n.deps().map { in =>
            fp(s"""$in -> $n""")
          }

          if (n.compositeValues.size > 0) {
            n.compositeValues.map { visitNode(_) }
            n.compositeValues.map { v => fp(s"""$n -> $v""") }
          }

        case n@DummyCtrlNode() =>
          fp(s"""$n""")
        case n: DummyMemNode =>
          fp(s"""$n""")

        case n: Wire => // Do nothing

        case n@OffChipArray(id, sizes @ _*) =>
          sizes.map(visitNode(_))
          fp(s"""subgraph cluster_$n {""")
          fp(s"""color=\"none\"""")
          fp(s"""label=\"\"""")
          fp(s"""$n [label=\"$n\" shape=\"square\" fontcolor=\"white\" color=\"white\" style=\"filled\" fillcolor=\"$offChipColor\"]""")
          for (s <- sizes) {
            fp(s"""$s -> $n""")
          }
          fp(s"""}""")

        case n@PE(deps @ _*) =>
          deps.map(visitNode(_))
          fp(s"""subgraph cluster_$n {""")
          fp(s"""color=\"none\"""")
          fp(s"""label=\"\"""")
          fp(s"""$n [label=\"$n\" shape=\"square\" fontcolor=\"white\" color=\"white\" style=\"filled\" fillcolor="black"]""")
          for (d <- deps) {
            fp(s"""$d -> $n""")
          }
          fp(s"""}""")
        case _ =>
          super.visitNode(node)

      }
    }
  }
}
