package dhdl.graph.traversal
import dhdl.graph._
import dhdl.Design
import dhdl.Config

import scala.collection.mutable.Set

abstract class Traversal(implicit val design: Design) {
  var isInit = false
  val visited = Set[Node]()
  val debug = false

  def msg(x: String) = if (Config.quick) () else System.out.println(x)
  def dbg(x: String) = if (debug && !Config.dse) System.out.println(x)

  def reset() {
    visited.clear()
    isInit = false
  }

  def run(node: Node) = {
    initPass()
    visitNode(node)
    finPass()
  }

  def initPass() = {
    isInit = true
  }

  def visitNode(node: Node) : Unit = {
    node match {
      case n@Fix2Float(op, t) =>
        visitNode(op)
      case n@Float2Fix(op, t) =>
        visitNode(op)
      case n@Not(op) =>
        visitNode(op)
      case n@Add(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Sub(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Mul(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Div(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Lt(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Gt(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Mux(sel, i1, i2) =>
        visitNode(sel)
        visitNode(i1)
        visitNode(i2)
      case n@Bundle(elems @_*) =>
        elems.map { visitNode(_) }
      case n@MinWithMetadata(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        visitNode(n.sel)
        visitNode(n.key)
        visitNode(n.metadata)
      case n@MaxWithMetadata(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
        visitNode(n.sel)
        visitNode(n.key)
        visitNode(n.metadata)
      case n@Max(op1, op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Eq(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Le(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Ge(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@And(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Or(op1,op2) =>
        visitNode(op1)
        visitNode(op2)
      case n@Abs(op) =>
        visitNode(op)
       case n@Exp(op) =>
        visitNode(op)
       case n@Log(op) =>
        visitNode(op)
       case n@Sqrt(op) =>
        visitNode(op)
     case n@ArgIn(id) =>
      case n@ArgOut(id,value) =>
        visitNode(value)
      case n@Ld(mem, addr, stride) =>
        visitNode(mem)
        visitNode(addr)
      case n@St(mem, addr, data, start, stride) =>
        visitNode(mem)
        visitNode(addr)
        visitNode(data)
      case n@Const(value) =>
      case n@Slice(in, from, to) =>
        visitNode(in)
      case n: Ctr =>
        for (max <- n.max) {
          visitNode(max)
        }
      case n@GraphNode(nodes @ _*) =>
        n.deps.map(visitNode(_))
        val sched = n.topSort
        sched.map(visitNode(_))
      case n@ReduceTree(oldval, accum, graph @_*) =>
        visitNode(oldval)
        visitNode(accum)
        graph.map { visitNode(_) }
      case n@CollectReduceTree(in, accum, body @_*) =>
        in.map { visitNode(_) }
        visitNode(accum)
        body.map { visitNode(_) }
      case n@Pipe(ctr, mapNode, redNode) =>
        visitNode(ctr)
        visitNode(mapNode)
        if (n.hasReduce) visitNode(redNode.get)
      case n@Parallel(nodes @ _*) =>
        for (node <- n.nodes) {
          visitNode(node)
        }
      case n: TileMemLd =>
        visitNode(n.baseAddr)
        visitNode(n.colDim)
        visitNode(n.idx0)
        visitNode(n.idx1)
        n.buf.map { visitNode(_) }
        visitNode(n.force)
      case n: TileMemSt =>
        visitNode(n.baseAddr)
        visitNode(n.colDim)
        visitNode(n.idx0)
        visitNode(n.idx1)
        visitNode(n.buf)
        visitNode(n.force)
      case n@MetaPipeline(ctr, regChain, numIter, nodes @ _*) =>
        visitNode(numIter)
        visitNode(ctr)
        visitNode(regChain)
        for (node <- n.nodes) {
          visitNode(node)
        }
      case n@Sequential(ctr, numIter, nodes @ _*) =>
        visitNode(numIter)
        visitNode(ctr)
        for (node <- n.nodes) {
          visitNode(node)
        }
      case n@BRAM(depth, isDblBuf) =>
      case n@PQ(depth, isDblBuf) =>
      case n@Reg(isDblBuf,init) =>
        n.deps.map {visitNode(_)}
      case n@DummyCtrlNode() =>
      case n: DummyMemNode =>
      case n: Wire =>
      case n@OffChipArray(_, sizes @ _*) =>
        sizes.map(visitNode(_))
      case n@PE(deps @ _*) =>
        deps.map(visitNode(_))
      case _ =>
        throw new Exception(s"Don't know how to visit $node")
    }
  }

  def finPass() = {}
}
