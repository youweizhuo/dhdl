package dhdl.graph

import scala.collection.mutable.Set
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import scala.math.max
import dhdl.transform.MPRewriteTransformer
import dhdl.analysis.MPDblBufAnalysis
import dhdl.Design

abstract class Type {
  def isBool: Boolean
  def isUInt: Boolean
  def bits: Int
}

/** Fixed point type representation with decimal and fractional bits.
 *  Defaults to an unsigned integer
 *  @param decimalBits: Number of bits to the left of decimal point
 *  @param fracBits: Number of bits to the right of decimal point
 *  @param signed: True for signed numbers, false for unsigned numbers
 */
case class FixPt(decimalBits: Int = 32, fracBits: Int = 0, signed: Boolean = false) extends Type {
  def isBool = (decimalBits == 1 && fracBits == 0)
  def isUInt = (fracBits == 0 && signed == false)
  def bits = decimalBits + fracBits
}

/**
 * Special forms of unapplies and constructors for special fixed point reps
 * (Note that unapply in Scala can return Boolean for simple matching)
 */

object UInt {
  def apply(dBits: Int = 32) = FixPt(dBits,0,false)
  def unapply(x: Any): Option[Int] = x match {
    case FixPt(dBits, 0, false) => Some(dBits)
    case _ => None
  }
}
object SInt {
  def apply(dBits: Int = 32) = FixPt(dBits,0,true)
  def unapply(x: Any): Option[Int] = x match {
    case FixPt(dBits, 0, true) => Some(dBits)
    case _ => None
  }
}
object Bool {
  def apply() = FixPt(1,0,false)
  def unapply(x: Any): Boolean = x match {
    case FixPt(1,0,_) => true
    case _ => false
  }
}

/** Floating point type representation. Defaults to single precision.
 *  @param mantissaBits: Number of mantissa bits
 *  @param expBits: Number of exponent bits
 */
case class FloatPt(mantissaBits: Int = 24, expBits: Int = 8) extends Type {
  def isBool = false
  def isUInt = false
  def bits = mantissaBits + expBits
}

object Flt {
  def apply() = FloatPt(24,8)
  def unapply(x:Any): Boolean = x match {
    case FloatPt(24,8) => true
    case _ => false
  }
}
object Dbl {
  def apply() = FloatPt(53,11)
  def unapply(x:Any): Boolean = x match {
    case FloatPt(53,11) => true
    case _ => false
  }
}

/** Composite type - a type that represents a collection of types
 */
case class CompositeType(ts: List[Type]) extends Type {
  def isUInt = false
  def isBool = false
  def bits = ts.map(_.bits).sum
}

/** Base class for all DHDL nodes. This class contains
  * implementations of common methods that every DHDL node
  * can use and override
  */
abstract class Node(implicit design: Design) extends Product {
  val name: String
  val stage: Int = design.stage
	design.addNode(this)

  /**
   *  Parent CtrlNode that this node belongs to
   */
  private var parent : Option[CtrlNode] = None
  def setParent(n: CtrlNode) { parent = Some(n) }
  def getParent() = parent.getOrElse { null }
  def hasParent() = parent.isDefined

  /** Dependencies of each node are tracked as
   *  parameters in the case class. This function iterates
   *  through the dependencies and returns a list. Note
   *  that this function is generic in that it returns a list
   *  containing every case class parameter, not just nodes.
   */
  def deps() = this.productIterator.flatMap{case x: Node => Some(x); case _ => None}.toList
  /*{
    val lb = ListBuffer[Any]()
    val iter = this.productIterator
    while (iter.hasNext) {
      val n = iter.next
      lb.append(n)
    }
    lb.toList
  }*/

  /**
   *  Get a list of all outputs from this node
   */
  def getouts() = List(this)

  override def toString = name
  override def equals(that: Any) = that match {
    case n: Node => super.equals(that) && name == n.name
    case _ => super.equals(that)
  }
}

case class Par(kind: String, degree: Int)

/** Base class for all nodes representing simple combinational logic.
 *  Every combinational node produces exactly ONE value
 */
abstract class CombNode(implicit design: Design) extends Node {
  val parInfo: Par = Par("Vec", 1)

  /** Type of value produced by this combinational node */
  val t: Type = UInt()

  /** Every Combinational node produces a vector of values, specify the width here */
  val vecWidth: Int = 1

  def getVecWidth(op1: CombNode, op2: CombNode) = {
    (op1, op2) match {
      case (n1: Const[_], n2: Const[_]) =>
        1
      case (n1: Const[_], n2: CombNode) =>
        n2.vecWidth
      case (n1: CombNode, n2: Const[_]) =>
        n1.vecWidth
      case (n1: CombNode, n2: Reg) =>
        n1.vecWidth
      case (n1: Reg, n2: CombNode) =>
        n2.vecWidth
      case (n1: CombNode, n2: CombNode) =>
        // Vector-scalar operations are okay
        if (n1.vecWidth == 1) {
          n2.vecWidth
        } else if (n2.vecWidth == 1) {
          n1.vecWidth
        } else {
          assert(n1.vecWidth == n2.vecWidth, s"""In ${this.toString}, vector widths for operands $op1 ${op1.vecWidth} and $op2 ${op2.vecWidth} do not match!""")
          n1.vecWidth
        }
    }
  }

  val compositeValues = t match {
    case ct: CompositeType =>
      ct.ts.map { m => Wire(m) }
    case _ =>
      List()
  }
  def get(x: Int): CombNode = compositeValues(x)

  def apply(elem: Int) = {
    if (elem == 0 && vecWidth == 1) {
      this
    } else {
      Slice(this, elem, elem)
    }
  }

  def apply(from: Int, to: Int) = {
    Slice(this, from, to)
  }
}

case class Slice(in: CombNode, from: Int, to: Int)(implicit design: Design) extends CombNode {
  override val name = s"slice_${design.nextId}"
  // Type check: In the current version, from == to (slice behaves as a getElement)
  if (from != to) {
    throw new Exception(s"""Slice start index $from must be <= end index (currently $to)""")
  }
  // Type check: from and to must both be <= in.vecWidth
  if (from > in.vecWidth || to > in.vecWidth) {
    throw new Exception(s"""Invalid slice indices $from, $to from $in (vecWidth ${in.vecWidth})""")
  }
  override val t = in.t

  override val vecWidth = to - from + 1
}
object Slice {
  def apply(in: CombNode, elem: Int)(implicit design: Design) = {
    new Slice(in, elem, elem)
  }
}
/** Base class for all nodes representing memory structures
 */
abstract class MemNode(implicit design: Design) extends Node with DblBufInterface {
  /** If true, this memory is to be implemented as two modules of the given size
   *  and operated as a double buffer
   */
  val isDblBuf = false

  /**
   *  If true, represents an accumulator
   */
  var accumNode = false
  def setAccum() { accumNode = true }
  def isAccum() = accumNode

  /** Type of value contained in each memory word */
  val t: Type = UInt()

  /** Number of banks: Set to max(readerVecWidth, writerVecWidth, #distinctWriters) */
  var banks: Int = 1

  /** BRAM stride - number of contiguous elements to be mapped to a single bank */
  var stride: Int = 1

  val stNodes = ListBuffer[St]()
}

/** Base class for all controller nodes. All controller nodes implement
 *  a standard control signal interface with which it can be stopped,
 *  started and polled on for completion.
 */
abstract class CtrlNode(implicit design: Design) extends Node {
  override def getouts() = List[Node]()
}

/** Dummy control node with no dependencies. Used for debugging purposes
 */
case class DummyCtrlNode()(implicit design: Design) extends CtrlNode {
  override val name = s"dummyCtrl_${design.nextId}"
}

/**
 *  Dummy memory node, used for debugging and in characterization
 *  benchmarks
 */
case class DummyMemNode(tp: Type = UInt())(implicit design: Design) extends MemNode {
  override val name = s"dummyMem_${design.nextId}"
  override val t = tp
}

/**
 *  FixPt -> FloatPt type conversion node
 */
case class Fix2Float(op: CombNode, floatt: FloatPt = FloatPt())(implicit design: Design) extends CombNode {
  override val name = s"fix2float_${design.nextId}"
  override val t = floatt

  // Typecheck - op's type has to be a FixPt
  assert(op.t.isInstanceOf[FixPt], s"Input $op (type ${op.t}) to Fix2Float does not have a FixPt type!")
}

/**
 * FloatPt -> FixPt type conversion node
 */
case class Float2Fix(op: CombNode, fixt: FixPt = UInt())(implicit design: Design) extends CombNode {
  override val name = s"float2fix_${design.nextId}"
  override val t = fixt

  // Typecheck - op's type has to be a FloatPt
  assert(op.t.isInstanceOf[FloatPt], s"Input $op (type ${op.t}) to Float2Fix is not in FloatPt!")
}

/** Adder (integer / floating point) that adds values produced
 *  by two other [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Add(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"add_${design.nextId}"
  override val t =  (op1.t, op2.t) match {
      case (FixPt(d1, f1, s1), FixPt(d2, f2, s2)) => FixPt(max(d1, d2), max(f1, f2), s1 | s2)
      case (FloatPt(m1, e1), FixPt(d2, f2, _)) => FloatPt(m1, e1)
      case (FixPt(d1, f1, _), FloatPt(m2, e2)) => FloatPt(m2, e2)
      case (FloatPt(m1, e1), FloatPt(m2, e2)) => FloatPt(max(m1, m2), max(e1, e2))
    }

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Subtracter (integer / floating point) that subtracts values produced
 *  by two other [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Sub(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"sub_${design.nextId}"
  override val t =  (op1.t, op2.t) match {
      case (FixPt(d1, f1, s1), FixPt(d2, f2, s2)) => FixPt(max(d1, d2), max(f1, f2), s1 | s2)
      case (FloatPt(m1, e1), FixPt(d2, f2, _)) => FloatPt(m1, e1)
      case (FixPt(d1, f1, _), FloatPt(m2, e2)) => FloatPt(m2, e2)
      case (FloatPt(m1, e1), FloatPt(m2, e2)) => FloatPt(max(m1, m2), max(e1, e2))
    }

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Multiplier (integer / floating point) that multiplies values produced
 *  by two other [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Mul(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"mul_${design.nextId}"
  override val t =  (op1.t, op2.t) match {
      case (FixPt(d1, f1, s1), FixPt(d2, f2, s2)) => FixPt(max(d1, d2), max(f1, f2), s1 | s2)
      case (FloatPt(m1, e1), FixPt(d2, f2, s2)) => FloatPt(m1, e1)
      case (FixPt(d1, f1, _), FloatPt(m2, e2)) => FloatPt(m2, e2)
      case (FloatPt(m1, e1), FloatPt(m2, e2)) => FloatPt(max(m1, m2), max(e1, e2))
    }

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Divider (integer / floating point) that divides values produced
 *  by two other [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Div(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"div_${design.nextId}"
  override val t =  (op1.t, op2.t) match {
      case (FixPt(d1, f1, s1), FixPt(d2, f2, s2)) => FixPt(d1, f1, s1 | s2)
      case (FloatPt(m1, e1), FixPt(d2, f2, s2)) => FloatPt(m1, e1)
      case (FixPt(d1, f1, _), FloatPt(m2, e2)) => FloatPt(m2, e2)
      case (FloatPt(m1, e1), FloatPt(m2, e2)) => FloatPt(m1, e1)
    }

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Compare if op1 is greater than op2 (integer / floating point),
 *  in which op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Gt(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"gt_${design.nextId}"
  override val t = Bool()

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Compare if op1 is greater than or equal to op2 (integer / floating point),
 *  in which op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Ge(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"ge_${design.nextId}"
  override val t = Bool()
  override val vecWidth = getVecWidth(op1, op2)
}

/** Compare if op1 is less than op2 (integer / floating point),
 *  in which op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Lt(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"lt_${design.nextId}"
  override val t = Bool()

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)

  // Type check: op1 and op2 must not be CompositeType nodes
  assert(!op1.t.isInstanceOf[CompositeType] & !op2.t.isInstanceOf[CompositeType], s"Error: Operands to Lt $op1 (${op1.t}) and $op2 (${op2.t}) must not be Composite Types")
}

/** Compare if op1 is less than or equal to op2 (integer / floating point),
 *  in which op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Le(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"le_${design.nextId}"
  override val t = Bool()
  override val vecWidth = getVecWidth(op1, op2)
}

/** Compare if op1 is equal to than op2 (integer / floating point),
 *  in which op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Eq(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"eq_${design.nextId}"
  override val t = Bool()

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = getVecWidth(op1, op2)
}

/** Bitwise inverse (not operation)
 *  @param op: Operand
 */
case class Not(op: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"not_${design.nextId}"
  override val t =  op.t

  // Type check: op1 and op2 must have the same vector width
  override val vecWidth = op.vecWidth
}

/** Bitwise and,
 *  op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class And(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"and_${design.nextId}"
  override val t =  op1.t
  override val vecWidth = getVecWidth(op1, op2)
}

object And {
	def apply(ops: CombNode*)(implicit design: Design):And = {
		ops.drop(2).foldLeft(And(ops(0),ops(1)):And){(ans,x) => And(ans,x)}
	}
}

/** Bitwise or,
 *  op1 and op2 are [[dhdl.graph.CombNode]]s.
 *  @param op1: First operand
 *  @param op2: Second operand
 */
case class Or(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"or_${design.nextId}"
  override val t =  op1.t
  override val vecWidth = getVecWidth(op1, op2)
}

/** Absolute value - return the absolute value of the given input
 *  @param op: Operand
 */
case class Abs(op: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"abs_${design.nextId}"
  override val t =  op.t
  override val vecWidth = op.vecWidth
}

/** Exp - calculate exponent
 *  @param op: Operand
 */
case class Exp(op: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"exp_${design.nextId}"
  override val t = FloatPt()
  override val vecWidth = op.vecWidth
}

/** Log - natural logarithm of operand
 *  @param op: Operand
 */
case class Log(op: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"log_${design.nextId}"
  override val t = FloatPt()
  override val vecWidth = op.vecWidth
}

/** Sqrt - Square root
 *  @param op: Operand
 */
case class Sqrt(op: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"sqrt_${design.nextId}"
  override val t = FloatPt()
  override val vecWidth = op.vecWidth

  // Typecheck: op must be float (this is a MaxJ limitation)
  assert(op.t.isInstanceOf[FloatPt], s"Input $op to Sqrt should be FloatPt, but is ${op.t} (MaxJ specific)")
}

/** Mux node: Selects between two [[dhdl.graph.CombNode]] values. More than two input Muxes
 *  are currently not supported.
 *  @param sel: Select signal
 *  @param i1: First Mux input
 *  @param i2: Second Mux input
 */
case class Mux(sel: CombNode, i1: CombNode, i2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"mux_${design.nextId}"

  // Type check: i1 and i2 must be of the same type
  assert(i1.t == i2.t, s"Mux inputs $i1 (${i1.t}) and $i2 (${i2.t}) do not have the same type")
  assert(sel.t.isBool, s"Select signal should be 1-bit, but has type ${sel.t}")

  override val t = i1.t

  // Type check: Currently allow only i1 and i2 nodes which have equal vecWidth
  override val vecWidth = math.max(getVecWidth(i1, i2), sel.vecWidth)
}

case class MinWithMetadata (op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"minWithMetadata_${design.nextId}"

  // Type check: Make sure that op1 and op2 are both tuples
  assert(op1.t.isInstanceOf[CompositeType] && op2.t.isInstanceOf[CompositeType], s"Invalid args to MinWithMetadata: $op1 (${op1.t}) and $op2 (${op2.t}) must both be tuples")

  // Type check: Make sure the composite types match
  assert(op1.t == op2.t, s"Invalid args to MinWithMetadata: Composite types don't match (${op1.t} and ${op2.t})")

  // Type of node == type of any of its operands
  override val t = op1.t
  override val vecWidth = op1.vecWidth

  // Define the body of MinWithMetadata here - quite haphazard at the moment
  // TODO: Refine this by using a GraphNode
  // TODO: Replace this after adding Min node
  val sel = Lt(op1.get(0), op2.get(0))
  val key = Mux(sel, op1.get(0), op2.get(0)) // Represents the minimum key
  val metadata = Mux(sel, op1.get(1), op2.get(1)) // Represents the metadata associated with the minimum key

  // 0 returns key, 1 returns metdata
  override def get(x: Int) = {
    if (x == 0) key
    else if (x == 1) metadata
    else {
      throw new Exception(s"Invalid idx $x used to index into MinWithMetadata")
      key
    }
  }
}

case class MaxWithMetadata(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"maxWithMetadata_${design.nextId}"

  // Type check: Make sure that op1 and op2 are both tuples
  assert(op1.t.isInstanceOf[CompositeType] && op2.t.isInstanceOf[CompositeType], s"Invalid args to MaxWithMetadata: $op1 (${op1.t}) and $op2 (${op2.t}) must both be tuples")

  // Type check: Make sure the composite types match
  assert(op1.t == op2.t, s"Invalid args to MaxWithMetadata: Composite types don't match (${op1.t} and ${op2.t})")

  // Type of node == type of any of its operands
  override val t = op1.t
  override val vecWidth = op1.vecWidth

  // Define the body of MaxWithMetadata here - quite haphazard at the moment
  // TODO: Refine this by using a GraphNode
  // TODO: Replace this after adding Min node
  val sel = Gt(op1.get(0), op2.get(0))
  val key = Mux(sel, op1.get(0), op2.get(0)) // Represents the maximum key
  val metadata = Mux(sel, op1.get(1), op2.get(1)) // Represents the metadata associated with the maximum key

  // 0 returns key, 1 returns metdata
  override def get(x: Int) = {
    if (x == 0) key
    else if (x == 1) metadata
    else {
      throw new Exception(s"Invalid idx $x used to index into MinWithMetadata")
      key
    }
  }
}

case class Max(op1: CombNode, op2: CombNode)(implicit design: Design) extends CombNode {
  override val name = s"max_${design.nextId}"

  // Type check: Make sure the composite types match
  assert(op1.t == op2.t, s"Invalid inputs to Max: Composite types don't match (${op1.t} and ${op2.t})")

  // Type of node == type of any of its operands
  override val t = op1.t
  override val vecWidth = op1.vecWidth
}

/** Argument passed from host CPU code to accelerator using a memory-mapped register.
 *  Represents scalar input to accelerator, and are read-only from accelerator.
 *  @param id: String identifier used by user to identify argument
 *  @param tp: Optional type of argument, defaults to 32-bit signed integer
 */
case class ArgIn(id: String)(implicit design: Design) extends ConstValueNode {
  override val name = s"""argIn_${id}"""
  val arg = design.nextId(this)

  if (design.checkIds) {
    assert(!design.argIds.contains(id), s"Argument ID $id is already in use")
    design.argIds += id
  }
}
object ArgIn {
  /** Companion object's apply method which takes in a type parameter for the argument */
  def apply(tp: Type)(id: String)(implicit design: Design) = new { override val t = tp } with ArgIn(id)
}

/** Scalar values passed from accelerator to host CPU code via memory-mapped registers.
 *  Represent scalar outputs from the accelerator, and are write-only from accelerator.
 *  @param id: String identifier used by user to identify argument
 *  @param value: Combinational node whose value is to be output to CPU
 */
case class ArgOut(id: String, value: CombNode)(implicit design: Design) extends CombNode {
  val arg = design.nextId(this)
  override val name = s"""argOut_${id}_$value"""
  override val t = value.t
  override def getouts() = List()

  if (design.checkIds) {
    assert(!design.argIds.contains(id), s"Argument ID $id is already in use")
    design.argIds += id
  }
}

/** Load from a memory. Currently only on-chip memory loads are supported. Loads
 *  from off-chip memories are not tested.
 *  @param mem: Memory ([[dhdl.graph.MemNode]]) from which data is to be loaded
 *  @param addr: Load address
 *  @param stride: Optional integer parameter specifying load access pattern
 */
case class Ld(mem: MemNode, addr: CombNode, stride: Int = 1)(implicit design: Design) extends CombNode {
  override val name = s"""ld_${design.nextId}"""

  override val t = mem.t

  // [HACK] Duplicated code with CombNode. This is necessary
  // because compositeValues is being initialized before t is
  // initialized here
  override val compositeValues = t match {
    case ct: CompositeType =>
      ct.ts.map { m => Wire(m) }
    case _ =>
      List()
  }

  // Type check: addr must be FixPt with no fractional part
  assert(addr.t.isUInt, "Address must be an unsigned integer")

  // Number of banks = Max(existing memory banks, vector width of incoming address stream, #distinctWriters)
  mem.banks = List(mem.banks, addr.vecWidth, mem.stNodes.size).max

  // Load access pattern should be known to BRAM
  mem.stride = stride

  override val vecWidth = addr.vecWidth
}

/** Store to a memory. Currently only on-chip memory stores are supported. Stores
 *  to off-chip memories are not tested. Addresses are treated as word addresses.
 *  Byte addresses are not supported.
 *  @param mem: Memory ([[dhdl.graph.MemNode]]) into which data is to be stored
 *  @param addr: Store address (word address)
 *  @param data: Data to be stored
 */
case class St(mem: MemNode, addr: CombNode, data: CombNode, start: Int = 0, stride: Int = 1)(implicit design: Design) extends CombNode {
  override val name = s"""st_${design.nextId}"""

  override val t = data.t

  // Type checks: data and mem must have the same type
  assert(data.t == mem.t, s"Attempting to store $data of type ${data.t} into $mem of type ${mem.t}")
  // Type check: addr must be FixPt with no fractional part
  assert(addr.t.isUInt, "Address must be an unsigned integer")

  // Type check: addr and data must have same vecWidth
  assert(addr.vecWidth == data.vecWidth, s"Address $addr (vecWidth ${addr.vecWidth}) and data $data ${data.vecWidth} do not have same vector width!")

  // Add self to mem's stNodes
  mem.stNodes += this

  // Number of banks = Max(existing memory banks, vector width of incoming address stream, #distinctWriters)
  mem.banks = List(mem.banks, addr.vecWidth, mem.stNodes.size).max

  override val vecWidth = data.vecWidth
  override def getouts() = List[Node]()
}

/**
 * Constant nodes - values that are known before the design is run
 */
abstract class ConstValueNode(implicit design: Design) extends CombNode

/** Constant value. This is a thin wrapper around the actual constant along with some
 *  type conversion logic.
 *  @param value: Constant value
 */
class Const[T](val value: T, tp: Type)(implicit design: Design) extends ConstValueNode {
  val id = design.nextId(this)
  override val name = s"""const_$id"""
  override val t = tp

  /** Two [[dhdl.graph.Const]] nodes are equal if their values are equal */
  /** ^^ Not entirely true - need to check types as well... */
  override def equals(that: Any) = that match {
    case that: Const[_] => this.value == that.value && this.t == that.t
    case _ => super.equals(that)
  }
  override def canEqual(that: Any) = that.isInstanceOf[Const[_]]
  override def productArity = 1
  override def productElement(n: Int) = List(value).apply(n)
}
object Const {
  // CSE for consts (no point in having 20 consts all for representing zero)
  def apply[T](value: T)(implicit design: Design) = {
    val t = value match {
      case x: Int => if (x < 0) SInt() else UInt()
      case _: Boolean => Bool()
      case _: Float => FloatPt(24, 8)
      case _: Double => FloatPt(53, 11)
      case _ => throw new Exception("Unsupported constant type. Only Int, Float and Double are supported")
    }
    design.consts.find{y => y.t == t && y.value == value}.getOrElse(new Const(value, t))
  }
  def unapply[T](x: Const[T]): Option[T] = Some(x.value)
}

/**
 * An unassigned wire used to represent scalar outputs prior to assignment
 * @param tp: The type of value represented by this wire
 */
case class Wire(tp: Type, par: Int = 1)(implicit design: Design) extends CombNode {
  override val name = s"""wire_${design.nextId}"""
  override val vecWidth = par
  override val t = tp
}

/**
 *  Bundle node. This is currently used to represent arbitrary value concatenations and
 *  multiple outputs from a single node (such as a [[dhdl.graph.Ctr]] node)
 *  @param n: An iterable [[scala.Seq]] of zero or more combinational values which are concatenated together.
 *  TODO: With zero parameters, this represents an unassigned wire, akin to a bound/local value.
 */
case class Bundle(n: CombNode*)(implicit design: Design) extends CombNode {
  override val name = s"""bundle_${design.nextId}"""
  override val t = CompositeType(n.map{_.t}.toList)
  override def get(x: Int) = {
    n(x)
  }

  override def deps() = n.toList
}

/** Counter node. Represents a chain of counters where each counter counts upto a certain max value. When
 *  the topmost counter reaches its maximum value, the entire counter chain ''saturates'' and does not
 *  wrap around.
 *  @param maxNStride: An iterable [[scala.Seq]] tuple of zero or more values.
 *  Each tuple represents the (max, stride) for one level in the loop.
 *  Maximum values and strides are specified in the order of topmost to bottommost counter.
 */
case class Ctr(par: List[Int])(maxNStride: (CombNode,Int)*)(implicit design: Design) extends CtrlNode {
  override val name = s"""ctr_${design.nextId}"""
  assert(par.size == maxNStride.size, s"""Not enough unroll factors provided: ${par.size} given ($par), ${maxNStride.size} required""")
  val ctrname = name
  val max = maxNStride.map(_._1)
  val stride = maxNStride.map(_._2)
  val ctrs = new Array[Wire](size).zipWithIndex.map { t =>
    val i = t._2
    val w = Wire(UInt(), par(i))
    w.setParent(this)
    w
  }
  def isUnitCtr = size != 0 && (max.head match {case Const(1) => (size == 1); case _ => false })
  def apply(x: Int) = ctrs(x)
  override def getouts() = ctrs.toList
  def size = max.size
}
object Ctr {
  def apply(maxNStride: (CombNode, Int)*)(implicit design: Design) = {
    val parList: List[Int] = (0 until maxNStride.size).map { i => 1 }.toList
    new Ctr(parList)(maxNStride:_*)
  }
}

/**
 *  Abstract container node for a set of CombNodes that form a graph.
 *  TODO: Design of GraphNode is a bit clumsy at the moment, for the
 *  following two reasons:
 *  1. Nodes can be added to GraphNode in two ways. One is via immutable
 *  arguments during construction, the other is using the add() method.
 *  Immutable arguments is used in [[dhdl.graph.Pipe]], while add() is used
 *  in [[dhdl.graph.Sequential]].
 *  2. Arguments to GraphNode do not represent dependencies; in fact, they
 *  represent members of the node. While this is inconsistent, this is also a
 *  common feature seen in other CtrlNodes like [[dhdl.graph.Sequential]] too.
 */
abstract class AbstractGraphNode(ns: CombNode*)(implicit design: Design) extends Node {
  val nodes = Set[Node](ns:_*)

  override def deps() = nodes.flatMap(_.deps()).filterNot(nodes contains _).toList

  override def getouts() = (nodes -- nodes.map { _.deps() }.flatten).toList

  override def setParent(n: CtrlNode) {
    super.setParent(n)
    nodes.foreach{_.setParent(n)}
  }

  /**
   *  Returns a list of nodes in topologically sorted order
   *  based on their dependencies (as returned by the deps() function)
   *  TODO: What to do for cycles? Always die?
   */
  def topSort = {
    // 0. Make a clone of all nodes
    val nodeClone = nodes.clone
    val schedList = ListBuffer[Node]()
    while (!nodeClone.isEmpty) {
      // 1. Discover nodes whose dependencies are not in the clone set ( O(n^2) )
      val indepSet = nodeClone.filterNot{n => n.deps.exists(nodeClone contains _) }

      // 1.5. Sanity check: empty leaf set means we have a cycle
      assert(!indepSet.isEmpty, s"ERROR: Found cycle in $name during topological sort")

      // 2. Remove leaves from clone set and insert into schedule list
      nodeClone --= indepSet
      schedList ++= indepSet

      // 3. While nodes are remaining in the clone, goto (1)
    }
    // 4. Done, return schedule list
    schedList.toList
  }

  def add(ns: Node*) { nodes ++= ns; ns.map { _.setParent(this.getParent()) } }

  def rm(n: Node) { nodes -= n }

  def isEmpty() = nodes.isEmpty

  override val name = s"graph_${design.nextId}"
}
object Block {
  def apply(x: => Any)(implicit design: Design) = {
    val prevNodes = design.allNodes.clone
    x
    val nodes= design.allNodes.filterNot(prevNodes contains _).filter(_.isInstanceOf[CombNode]).asInstanceOf[Set[CombNode]]
    GraphNode(nodes.toSeq:_*)
  }
}


/**
 *  Generic container with a set of CombNodes. Currently, the only use case for
 *  this class is to represent Map and Reduce portions of [[dhdl.graph.Pipe]] bodies.
 */
case class GraphNode(ns: CombNode*)(implicit design: Design) extends AbstractGraphNode(ns:_*)

case class ReduceTree(oldval: CombNode, accum: Reg, graph: GraphNode*)(implicit design: Design) extends AbstractGraphNode() {
  override val name = s"reduceTree_${design.nextId}"
  val par = oldval.vecWidth
  graph.map { g => add(g.nodes.toList:_*) }

  override def setParent(n: CtrlNode) = {
    super.setParent(n)
    graph.map { g => g.setParent(n) }
  }

  def contains(n: Node) = {
    val nodeInGraph = graph.map { _.nodes.contains(n) }.reduce(_|_)
    nodeInGraph | (n == accum)
  }
}
object ReduceTree {
  def apply(oldval: CombNode, accum: Reg, node: CombNode)(implicit design: Design) = new ReduceTree(oldval, accum, GraphNode(node))
}

/**
 *  Represents a reduction of a collection. Body is typically a Pipe which only contains a MapNode
 *  producing values into an accumulator
 *  @param in: MemNode containing collection to be reduced with accumulator
 *  @param accum: Accumumator MemNode
 *  @param body: CtrlNode that reads from in and accumto write new values into accum
 */
case class CollectReduceTree(in: List[BRAM], accum: BRAM, body: CtrlNode*)(implicit design: Design) extends CtrlNode {
  override val name = s"collectReduceTree_${design.nextId}"
  val par = body.size
  accum.setAccum()
  override def setParent(n: CtrlNode) = {
    super.setParent(n)
    body.map { b => b.setParent(n) }
  }
}
object CollectReduceTree {
  def apply (in: BRAM, accum: BRAM, body: CtrlNode*)(implicit design: Design) = {
    new CollectReduceTree(List(in),accum, body:_*)
  }
}

/** Represents a hardware pipeline consisting purely of combinational nodes
 *  @param ctr: Counter object ([[dhdl.graph.Ctr]]) that generates loop iterators
 */
case class Pipe(ctr: Ctr, mapNode: GraphNode, reduceNode: Option[ReduceTree] = None)(implicit design: Design) extends CtrlNode {
  override val name = s"pipe_${design.nextId}"
  def isUnitPipe = ctr.isUnitCtr
  def hasReduce = reduceNode.isDefined
  def iters = numIter.getouts.head.asInstanceOf[CombNode]

  // Graph of nodes that compute the total number of iterations (pipe's is used only for bounds analysis)
  val numIter = GraphNode()
  numIter.setParent(this)

  val allDivs = (0 until ctr.size) map { i =>
    val max = ctr.max(i)
    val stride = ctr.stride(i)
    val par = ctr.par(i)
    val div:CombNode = if (stride == 1) Div(max, Const(par)) else Div(max, Const(stride*par))
    div.setParent(this)
    numIter.add(div)
    div
  }
  val mul = allDivs.reduce { (d1,d2) =>
    val m = Mul(d1,d2)
    m.setParent(this)
    numIter.add(m)
    m
  }

  ctr.setParent(this)
  val par = ctr.par
  mapNode.setParent(this)
  if (hasReduce) reduceNode.get.setParent(this)
  markAccumMems()

  def markAccumMems() = {
    val ldmems = mapNode.nodes.filter(_.isInstanceOf[Ld]).toList.asInstanceOf[List[Ld]].map(_.mem).toSet
    val stmems = mapNode.nodes.filter(_.isInstanceOf[St]).toList.asInstanceOf[List[St]].map(_.mem).toSet
    val intersect = ldmems.intersect(stmems)
    intersect.map(_.setAccum())
  }

  // Check: Disallow Map nodes to have dependencies on reduce nodes
  val mapdeps = mapNode.deps
  val mapDependsOnReduce = if (hasReduce) {
    mapdeps.filter{ d => (hasReduce & reduceNode.get.contains(d)) }
  } else {
    List()
  }
  assert(mapDependsOnReduce.size == 0,
    s"""
      Illegal pipe $name: A dependency to a CombNode in reduceNode ${reduceNode.get} ($mapDependsOnReduce) exists in mapNode
      MapNode in Pipes cannot have dependencies on their reduceNodes
    """)
}
object Pipe {
  def apply(nodes: CombNode*)(implicit design: Design)
    = new Pipe(Ctr((Const(1),1)), GraphNode(nodes:_*), None)
  def apply(mapNode: GraphNode, reduceNode: ReduceTree)(implicit design: Design)
    = new Pipe(Ctr((Const(1),1)), mapNode, Some(reduceNode))
  def apply(mapNode: GraphNode, reduceNode: Option[ReduceTree])(implicit design: Design)
    = new Pipe(Ctr((Const(1),1)), mapNode, reduceNode)
  def apply(ctr: Ctr, mapNode: GraphNode, reduceNode: ReduceTree)(implicit design: Design)
    = new Pipe(ctr, mapNode, Some(reduceNode))
  def apply(blk: => Any)(implicit design: Design)
    = new Pipe(Ctr((Const(1),1)), Block(blk), None)

  def apply(maxNStride: (CombNode,Int)*)(blk: Ctr => Any)(implicit design: Design) = {
    val ctr = Ctr(maxNStride:_*)
    new Pipe(ctr, Block{ blk(ctr) }, None)
  }
}

/**
 *  Temporarily disabled as the current definition assumes that reduce is a GraphNode
 *  while reduce is now a ReduceTree, where the input and accum must be explicitly
 *  specified
 */
//object PipeReduce {
//  def apply(maxNStride: (CombNode,Int)*)(blk: Ctr => Any)(reduce: Ctr => Any) = {
//    val ctr = Ctr(maxNStride:_*)
//    new Pipe(ctr, Block{ blk(ctr) }, Block{ reduce(ctr) })
//  }
//}


/** Represents a MetaPipeline. A MetaPipeline is a hierarchical pipeline
 *  which has a list of stages. Each stage has to be a [[dhdl.graph.CtrlNode]] in
 *  the current implementation. Nodes passed in as arguments are executed in a pipelined
 *  fashion. Values that are used by multiple stages (like counter values) should be
 *  handled at code generation time using double-buffered registers.
 *  @param ctr: Counter object ([[dhdl.graph.Ctr]]) that generates loop iterators
 *  @param nodes: An iterable [[scala.Seq]] of zero or more [[dhdl.graph.CtrlNode]]s
 *  representing the stages of the MetaPipeline. The nodes have to be distinct; passing
 *  the same [[dhdl.graph.CtrlNode]] twice or more results in an error.
 */
case class MetaPipeline(ctr: Ctr, regChain: GraphNode, numIter: GraphNode, nodes: CtrlNode*)(implicit design: Design) extends CtrlNode {
  override val name = s"metapipe_${design.nextId}"
  def size = nodes.size
  def iters = numIter.getouts.head.asInstanceOf[CombNode]

  ctr.setParent(this)
  regChain.setParent(this)
  numIter.setParent(this)

  assert(nodes.length == nodes.distinct.length, "'Metapipeline' node members must not be aliases of each other")

  nodes.map(_.setParent(this))
}
object MetaPipeline {
  def apply(ctr: Ctr, nodes: CtrlNode*)(implicit design: Design) = {
    val regChain = GraphNode()
    val numIter = Block {
      val allDivs = (0 until ctr.size) map { i =>
        val max = ctr.max(i)
        val stride = ctr.stride(i)
        val par = ctr.par(i)
        if (stride == 1) Div(max, Const(par)) else Div(max, Const(stride*par))
      }
      allDivs.asInstanceOf[Vector[CombNode]].reduce { (d1,d2) => Mul(d1,d2) }
    }
    new MetaPipeline(ctr, regChain, numIter, nodes:_*)
  }
}


/** Represents sequential control flow. Each stage has to be a [[dhdl.graph.CtrlNode]] in
 *  the current implementation. Nodes passed in as arguments are executed in sequence.
 *  Values that are used by multiple stages (like counter values) should be
 *  handled at code generation time using double-buffered registers.
 *  @param ctr: Counter object ([[dhdl.graph.Ctr]]) that generates loop iterators
 *  @param nodes: An iterable [[scala.Seq]] of zero or more [[dhdl.graph.CtrlNode]]s
 *  representing the stages to be executed sequentially. The nodes have to be distinct; passing
 *  the same [[dhdl.graph.CtrlNode]] twice or more results in an error.
 */
case class Sequential(ctr: Ctr, numIter: GraphNode, nodes: CtrlNode*)(implicit design: Design) extends CtrlNode {
  override val name = s"sequential_${design.nextId}"
  def size = nodes.size
  def iters = numIter.getouts.head.asInstanceOf[CombNode]

  numIter.setParent(this)

  ctr.setParent(this)
  nodes.foreach(_.setParent(this))

  assert(nodes.length == nodes.distinct.length, "'Sequential' node members must not be aliases of each other")
}
object Sequential {
  def apply(ctr: Ctr, nodes: CtrlNode*)(implicit design: Design) = {
    val numIter = Block {
      val allDivs = (0 until ctr.size) map { i =>
        val max = ctr.max(i)
        val stride = ctr.stride(i)
        val par = ctr.par(i)
        if (stride == 1) Div(max, Const(par)) else Div(max, Const(stride*par))
      }
      allDivs.asInstanceOf[Vector[CombNode]].reduce{ (d1,d2) => Mul(d1,d2) }
    }
    new Sequential(ctr, numIter, nodes:_*)
  }
  def apply(nodes: CtrlNode*)(implicit design: Design) = {
    // NOTE: Block(Const(1)) doesn't work if Const(1) has been created already...
    new Sequential(Ctr((Const(1),1)), GraphNode(Const(1)), nodes:_*)
  }
}

/** Represents parallel fork-join style control flow. Each node has to be a [[dhdl.graph.CtrlNode]] in
 *  the current implementation. Nodes passed in as arguments are executed in parallel.
 *  This node automatically synchronizes the execution of all the nodes.
 *  @param nodes: An iterable [[scala.Seq]] of zero or more [[dhdl.graph.CtrlNode]]s
 *  representing the stages to be executed in parallel. The nodes have to be distinct; passing
 *  the same [[dhdl.graph.CtrlNode]] twice or more results in an error.
 */
case class Parallel(nodes: CtrlNode*)(implicit design: Design) extends CtrlNode {
  override val name = s"parallel_${design.nextId}"
  def size = nodes.size

  assert(nodes.length == nodes.distinct.length, "'Parallel' node members must not be aliases of each other")
  nodes.foreach(_.setParent(this))
}

/** Memory command generator to load a 2-dimensional Tile of data from off-chip memory
 *  @param baseAddr: Off-chip array from which to load data from
 *  @param colDim: Width of off-chip array's column in number of elements. This is required to compute the tile offset correctly.
 *  @param idx0: Starting row of tile in off-chip array
 *  @param idx1: Starting column of tile in off-chip array
 *  @param buf: On-chip buffer to load the tile of data into
 *  @param tileDim0: Number of rows in tile
 *  @param tileDim1: Number of columns in tile
 *  @param force: When true, forces loading of a tile when module is enabled. When false,
 *  issue load only when atleast one of idx0 or idx1 changes. This is true by default.
 */
case class TileMemLd(baseAddr: OffChipArray, colDim: CombNode, idx0: CombNode, idx1: CombNode, buf: List[MemNode], tileDim0: Int, tileDim1: Int)(implicit design: Design) extends CtrlNode {
  override val name = s"tilememld_${design.nextId}"

  private var _force: CombNode = Const(false)
  val isLoading = Wire(Bool())
  isLoading.setParent(this)

  // Type check: baseAddr and buf must have the same type
  baseAddr.t match {
    case ct: CompositeType =>
      assert(buf.size == ct.ts.size, s"Wrong number of BRAMs passed to TileMemLd: Required ${ct.ts.size}, passed ${buf.size}. OffChipArray type is ${baseAddr.t}")
      buf.zip(ct.ts).map { tuple =>
        assert(tuple._1.t == tuple._2, s"Type mismatch in TileMemLd: ${tuple._1}(${tuple._1.t}) != ${tuple._2}")
      }
    case _ =>
      assert(buf.size == 1, s"Too many BRAMs passed to TileMemLd: Required 1, passed ${buf.size}. OffChipArray type is ${baseAddr.t}")
      assert(baseAddr.t == buf(0).t, s"OffChipArray type '${baseAddr.t}' and buf type '${buf(0).t}' do not match!")
  }

  // Type check: idx0 and idx1 must be scalars (vecWidth == 1)
//  assert(idx0.vecWidth == 1, s"Idx0 $idx0 has a vecWidth > 1 (${idx0.vecWidth})!")
//  assert(idx1.vecWidth == 1, s"Idx0 $idx1 has a vecWidth > 1 (${idx1.vecWidth})!")

  def force = _force
  def force_=(x: CombNode) {
    assert(force.t.isBool, s"Force input $force has incompatible type ${force.t}")
    _force = x
  }
  def withForce(x: CombNode) = {this.force = x; this}
  var contention = 1 // bit of a hack - this should actually be metadata (external to node?)
}
object TileMemLd {
  def apply(baseAddr: OffChipArray, colDim: CombNode, idx0: CombNode, idx1: CombNode, buf: MemNode, tileDim0: Int, tileDim1: Int)(implicit design: Design) = {
    new TileMemLd(baseAddr, colDim, idx0, idx1, List(buf), tileDim0, tileDim1)
  }
  def apply(baseAddr: OffChipArray, colDim: CombNode, idx0: CombNode, idx1: CombNode, buf: MemNode, tileDim0: Int, tileDim1: Int, force: CombNode)(implicit design: Design) = {
    new TileMemLd(baseAddr, colDim, idx0, idx1, List(buf), tileDim0, tileDim1).withForce(force)
  }
}

/** Memory command generator to store a 2-dimensional Tile of data to off-chip memory
 *  @param baseAddr: Off-chip array to which data is to be stored
 *  @param colDim: Width of off-chip array's column in number of elements. This is required to compute the tile offset correctly.
 *  @param idx0: Starting row of tile in off-chip array
 *  @param idx1: Starting column of tile in off-chip array
 *  @param buf: On-chip buffer containing the tile to be stored
 *  @param tileDim0: Number of rows in tile
 *  @param tileDim1: Number of columns in tile
 *  @param force: When true, forces storing of a tile when module is enabled. When false,
 *  issue store only when atleast one of idx0 or idx1 changes. This is true by default.
 */
case class TileMemSt(baseAddr: OffChipArray, colDim: CombNode, idx0: CombNode, idx1: CombNode, buf: MemNode, tileDim0: Int, tileDim1: Int)(implicit design: Design) extends CtrlNode {
  override val name = s"tilememst_${design.nextId}"

  private var _force: CombNode = Const(false)
  val isStoring = Wire(Bool())
  isStoring.setParent(this)

  // Type check: baseAddr and buf must have the same type
  assert(baseAddr.t == buf.t, s"OffChipArray type '${baseAddr.t}' and buf type '${buf.t}' do not match!")

  // Type check: idx0 and idx1 must be scalars (vecWidth == 1)
//  assert(idx0.vecWidth == 1, s"Idx0 $idx0 has a vecWidth > 1 (${idx0.vecWidth})!")
//  assert(idx1.vecWidth == 1, s"Idx0 $idx1 has a vecWidth > 1 (${idx1.vecWidth})!")

  def force = _force
  def force_= (x: CombNode) {
    assert(force.t.isBool, s"Force input $force has incompatible type ${force.t}")
    _force = x
  }
  def withForce(x: CombNode) = {this.force = x; this}
  var contention = 1 // bit of a hack - this should actually be metadata (external to node?)
}
object TileMemSt {
  def apply(baseAddr: OffChipArray, colDim: CombNode, idx0: CombNode, idx1: CombNode, buf: MemNode, tileDim0: Int, tileDim1: Int, force: CombNode)(implicit design: Design) = {
    new TileMemSt(baseAddr, colDim, idx0, idx1, buf, tileDim0, tileDim1).withForce(force)
  }
}


trait DblBufInterface {
  private val readers = ListBuffer[CtrlNode]()
  private var writer: Option[CtrlNode] = None

  def addReader(node: CtrlNode) { readers.append(node) }
  def clearReaders() { readers.clear }
  def setWriter(node: CtrlNode) { writer = Some(node) }

  def getReaders() = readers
  def getWriter() = writer
}

/** Block RAM
 *  @param depth: Number of words
 *  @param dblBuf: Double buffer if true, else regular BRAM
 */
case class BRAM(depth: Int, dblBuf: Boolean = false)(implicit design: Design) extends MemNode {
  override val name = if (dblBuf) s"dblbuf_${design.nextId}" else s"bram_${design.nextId}"
  override val isDblBuf = dblBuf
  val id = name

  if (design.checkIds) {
    assert(!design.memIds.contains(id), s"Memory ID $id is already in use")
    design.memIds += id
  }
}
object BRAM {
  def apply(tp: Type, depth: Int, dblBuf: Boolean)(implicit design: Design)
    = new { override val t = tp } with BRAM(depth, dblBuf)
  def apply(tp: Type, name: String, depth: Int, dblBuf: Boolean)(implicit design: Design)
    = new { override val t = tp; override val id = name } with BRAM(depth, dblBuf)
}

/** Priority Queue - currently stores elements in ascending order
 *  @param depth: Number of words
 *  @param dblBuf: Double buffer if true, else regular BRAM
 */
case class PQ(depth: Int, dblBuf: Boolean = false)(implicit design: Design) extends MemNode {
  override val name = if (dblBuf) s"dblbuf_pq_${design.nextId}" else s"pq_${design.nextId}"
  override val isDblBuf = dblBuf
  val id = name

  // By default, PQ stores a tuple: key, metadata
  override val t:Type = CompositeType(List(UInt(), UInt()))

  if (design.checkIds) {
    assert(!design.memIds.contains(id), s"Memory ID $id is already in use")
    design.memIds += id
  }
}
object PQ {
  def apply(tp: Type, depth: Int, dblBuf: Boolean)(implicit design: Design) = {
    assert(tp.isInstanceOf[CompositeType], s"Error: PQ can be created only with composite types (given type $tp)")
    new PQ(depth, dblBuf) { override val t = tp }
  }
  def apply(tp: Type, name: String, depth: Int, dblBuf: Boolean)(implicit design: Design) = {
    assert(tp.isInstanceOf[CompositeType], s"Error: PQ can be created only with composite types (given type $tp)")
    new PQ(depth, dblBuf) { override val t = tp; override val id = name }
  }
}

/** Register. This is not classified as a memory node, so it cannot be accessed
 *  using Ld/St. Register defines its own read/write interface.
 *  The 'wen' and 'rst' signals of the register are defined by its parent CtrlNode.
 *  It is currently the programmer's responsibility to set the parent (Using [[dhdl.graph.Register.setParent()]])
 *  The register behavior is controlled by the parent as follows:
 *    1. Write enable: Register.wen = Reg.input.getParent
 *    2. Reset: Register.rst = 1 when parent is enabled
 *
 *  @param dblBuf: Double buffered register if true, else regular register
 */
case class Reg(dblBuf: Boolean = false, init: Option[Const[_]] = None)(implicit design: Design) extends CombNode with DblBufInterface {

  override val t: Type = FixPt()
  override val name = s"reg_${design.nextId}"

  val isDblBuf = dblBuf
  private var in: Option[CombNode] = None

  def write(value: CombNode) = {
    assert(value.t == t, s"Register $name has type $t, attempting to write value of type ${value.t}")
    assert(in.isEmpty, s"Register $name already has its input set to $in, attempting to set it again with $value!")
    in = Some(value)
  }
  def input = in.getOrElse{throw new Exception("Register input has not yet been set")}

  def read() = this

  override def deps() = List(input)

  def producer() = input.getParent()
  def consumer() = {
    assert(isDblBuf, "Consumer valid only for double buffered reg")
    this.getParent()
  }

  val id: String = name

  assert(init.isEmpty || init.get.t == this.t, s"Type of reset value ${init.get} (${init.get.t}) and type of register $name ($t) must match")
}
object Reg {
  def apply(init: Const[_])(implicit design: Design)
    = new { override val t = init.t } with Reg(false,Some(init))

  def apply(dblBuf: Boolean, init: Const[_])(implicit design: Design)
    = new { override val t = init.t } with Reg(dblBuf,Some(init))

  def apply(tp: Type)(implicit design: Design)
    = new { override val t = tp } with Reg(false)
  def apply(tp: Type, dblBuf: Boolean)(implicit design: Design)
     = new { override val t = tp } with Reg(dblBuf)
  def apply(tp: Type, name: String, dblBuf: Boolean)(implicit design: Design)
    = new { override val t = tp; override val id = name } with Reg(dblBuf)
  def apply(name: String, dblBuf: Boolean)(implicit design: Design)
     = new { override val id = name } with Reg(dblBuf)
  def apply(vec: Int, dblBuf: Boolean)(implicit design: Design)
    = new { override val vecWidth = vec } with Reg(dblBuf)
  def apply(vec: Int, tp: Type, dblBuf: Boolean)(implicit design: Design)
    = new { override val vecWidth = vec; override val t = tp } with Reg(dblBuf)
  def apply(tp: Type, dblBuf: Boolean, init: Const[_])(implicit design: Design)
     = new { override val t = tp } with Reg(dblBuf, Some(init))
  def apply(tp: Type, dblBuf: Boolean, init: Option[Const[_]])(implicit design: Design)
     = new { override val t = tp } with Reg(dblBuf, init)
  def apply(vec: Int, tp: Type, dblBuf: Boolean, init: Option[Const[_]])(implicit design: Design)
     = new { override val vecWidth = vec; override val t = tp } with Reg(dblBuf, init)
}

/** Off-chip memory array. Currently, this node represents regular, dense
 *  N-dimensional data arrays that are stored off-chip.
 *  To represent a multi-dimensional array, size of each dimension must be
 *  specified as a separate argument. Note that the current
 *  implementation requires that the off-chip array dimensions are necessarily
 *  passed in from host CPU code as an [[dhdl.graph.ArgIn]] node. The assumption
 *  behind this design is that off-chip arrays are all populated by a bulk data
 *  copy from the CPU after initialization. While this is true in all the benchmarks
 *  considered so far, this can be a limitation if the design has to dump some
 *  intermediate off-chip data that does not have to be read by the CPU.
 *  @param id: String identifier used to identify off-chip array. This is for programmer
 *  convenience only
 *  @param sizes: An iterable [[scala.Seq]] of zero or more [[dhdl.graph.ArgIn]] nodes that
 *  each represent the size of the off-chip array along one dimension.
 */
case class OffChipArray(id: String, sizes: ConstValueNode*)(implicit design: Design) extends MemNode {
  override val name = s"offchip_${id}_${design.nextId}"

  if (design.checkIds) {
    assert(!design.memIds.contains(id), s"Memory ID $id is already in use")
    design.memIds += id
  }
}

object OffChipArray {
  def apply(tp: Type)(id: String, sizes: ConstValueNode*)(implicit design: Design) = {
    new { override val t = tp } with OffChipArray(id, sizes:_*)
  }
}
